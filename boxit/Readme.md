# Shell Library
## Abstract Layer for IHM

### The purposes...

According to tput and terminfo/termcap man pages we should use smac/rmacs routine:

The basics forms :
------------------

| tput code   |  result     |
|-------------|-------------|
| l= w=┬ k=┐  | ```┌┬┐```   |
| t=├ n=┼ u=┤ | ```├┼┤```   |
| m=└ v=┴ j=┘ | ```└┴┘```   |


With length and height
----------------------

| tput code | result |
|-----------|--------|
| x= │q=─ | │─       |


 We finally obtain any boxes forms we need

```
 l14qk    ┌────────────────────────────────────┐
 t.....t  │          CARD #17                  │
 l7qw7qk  ┌───┬────────────────────────────────┐
 t.t...t  │ID │    NAME : `4ed`                │
 t7qn7qu  ├───┼────────────────────────────────┤
 t.t...t  │ 1 │    JOHN .................OK    │
 t.t...t  │ 2 │    HENRY.................OK    │
 t.t...t  │ 3 │    HARRISON..............OK    │
 t.t...t  │ 4 │    "T"...................KO    │
 m7qv7qj  └───┴────────────────────────────────┘
```

 Combinaison of base element with length and height give us differents Boxes type:

#### Simple Box

```bash
┌─────────────┐
│ Simple Box  │ #This is it!
└─────────────┘
```
We obtain that with
```bash
                                                        <--${cpt}-->
 simpltop  : ƒ(l,q,k) = l+${cpt}(k)+q                  ┌─────────────┐
 simplside : ƒ(x,${text})= x+${text}+x                 │   ${text}   │ xN
 simplbutt : ƒ(m,q,j)= m+${cpt}(q)+j                   └─────────────┘
 with ${cpt}E[1,n]
```

┌──────────────┬───────┐
│Simple Multi  │  Box  │
└──────────────┴───────┘
```
 multitop :ƒ(l,w,k,q)=l+${cptA}[${cptB}(q)+w+${cptC}(q)]+k

 [${cptB}(q)+w+${cptC}(q)]
		________^________
	  /                   \
 ┌─────────┬────────┬────┬────────┐


multisid :ƒ(x,${text})=x+${A}+x+${B}+...+${Z}+x
│ ${A}   │${B}    │...│${Z}    │


multibut :ƒ(m,v,j,q)=m+${cptA}[${cptB}(q)+v+${cptC}(q)]+j
 └─────────┴────────┴───┴─────────┘


 with ${cpti}E[1,n] and iE[1,3]


┌─────────┬────────┬────┬────────┐
│ ${A}    │ ${B}   │... │${Z}    │
└─────────┴────────┴────┴────────┘
```


#### Simple Multi Head Box
```
 multitop : ƒ(l,w,k,q)   = l+${cptA}[${cptB}(q)+w+${cptC}(q)]+k
 multisid : ƒ(x,${text}) = x+${textA}+x+${textB}+...+${textZ}+x
 multijoi : ƒ(t,n,u)	  = t+${cptA}[${cptB}(q)+v+${cptC}(q)]+u

┌─────────┬─────────┬───┬─────────┐
│${textA} │${textB} │...│${textZ} │
├─────────┴─────────┴───┴─────────┤
```

#### Full Columns Multi Head Box

```
multitop : ƒ(l,w,k,q)   = l+${cptA}[${cptB}(q)+w+${cptC}(q)]+k
multisid:ƒ(x,${text})   =x+${textA}+x+${textB}+...+${textZ}+x
multijoi:ƒ(t,n,u)	    = t+${cptA}[${cptB}(q)+n+${cptC}(q)]+u

 ┌────────┬────────┬───┬────────┐
 │${textA}│${textB}│...│${textZ}│ 
 ├────────┼────────┼───┼────────┤ 

```
#### Simple Multi Bottom Box
```
 multitop : ƒ(l,w,k,q)   = t+${cptA}[${cptB}(q)+w+${cptC}(q)]+u
 multisid : ƒ(x,${text}) = x+${textA}+x+${textB}+...+${textZ}+x
 multibut : ƒ(m,v,j,q)   = m+${cptA}[${cptB}(q)+v+${cptC}(q)]+j

├────────┬────────┬───┬────────┤
│${textA}│${textB}│...│${textZ}│
└────────┴────────┴───┴────────┘
```

#### Full Columns Multi Bottom Box
```
 multitop : ƒ(l,w,k,q)   = t+${cptA}[${cptB}(q)+n+${cptC}(q)]+u
 multisid : ƒ(x,${text}) = x+${textA}+x+${textB}+...+${textZ}+x
 multibut : ƒ(m,v,j,q)   = m+${cptA}[${cptB}(q)+v+${cptC}(q)]+j

 ├────────┼────────┼───┼────────┤
 │${textA}│${textB}│...│${textZ}│
 └────────┴────────┴───┴────────┘
```

#### Simple BodyBox
```
 simple side 	:ƒ(x,${text})=x+${text}+x
 with ${text}length=${cptB}+${cptC}+...

 │<----------lenght------------>│
```

#### Full Columns BodyBox
```
 fullmultisid: ƒ(x,${text})=x+${textA}+x+${textB}+...+${textZ}+x

 │${textA}│${textB}│...│${textZ}│
```

The minimalist tput give us also this last glyphes : 
a=▒ y=≤ #z=≥ #r=⎼ #i=↓ #o=⎺ #p=⎻ #s=⎽ #d=r #f=° #g=± #h=n #b=→ #c=ƒ 


 ________

## SINGLE MODE  

Draw the Null Head Box consist in concatenate character in 'alternate character set'.
See the [String Capabilities](https://www.gnu.org/software/termutils/manual/termutils-2.0/html_chapter/tput_1.html) of the * http://www.gnu.org page

Code   | Basic Patern Single
-------|-------------------
 l q k | ┌───────────────┐
 x   x | │     blabla    │
 m q j | └───────────────┘

```
lqk┌──────────────┐
x x│<--length---->│
mqj└──────────────┘
```

Code | The Single Head |Code |The Single Bottom |Code |The Single Full
-----------------------|-----|------------------|-----|----------------
 lqk | ┌──────────────┐| lqk | ┌──────────────┐ | lqk | ┌──────────────┐
 tqu | ├──────────────┤| x x | │▒▒▒▒▒▒▒▒▒▒▒▒▒▒│ | tqu | ├──────────────┤
 x x | │▒▒▒▒▒▒▒▒▒▒▒▒▒▒│| x x | │<---length--->│ | x x | │<---length--->│
 x x | │<--length---->│| tqu | ├──────────────┤ | tqu | ├──────────────┤
 mqj | └──────────────┘| mqj | └──────────────┘ | mqj | └──────────────┘

```
┌──────────────┐┌──────────────┐┌──────────────┐
├──────────────┤│▒▒▒▒▒▒▒▒▒▒▒▒▒▒│├──────────────┤
│▒▒▒▒▒▒▒▒▒▒▒▒▒▒││<---length--->││<---length--->│
│<--length---->│├──────────────┤├──────────────┤
└──────────────┘└──────────────┘└──────────────┘
```

## MULTI MODE  

Code   | Basic Patern Multi|Code   | The Multi Bottom Box
-------|-------------------|-------|-------------------
 lqwqk | ┌───────┬───────┐ | lqqqk | ┌───────────────┐
 tqnqu | ├───────┼───────┤ | x...x | │▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒│
 m v j | └───────┴───────┘ | tqwqu | ├───────┬───────┤
                           || mqvqj | └───────┴───────┘  


Code  |The Multi Head Box |Code   | The Full Multi Head
------|-------------------|-------|--------------------
lqwqk | ┌───────┬───────┐ | lqwqk | ┌───────┬───────┐
tqnqu | ├───────┴───────┤ | tqnqu | ├───────┼───────┤
x...x | │▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒│ | x x x | │▒▒▒▒▒▒▒│▒▒▒▒▒▒▒│
mqqqj | └───────────────┘ | mqvqj | └───────┴───────┘




Code | The Multi Full Box
-----|-----------------------
lqwqk| ┌───────┬───────┬──...┐
tqnqu| ├───────┼───────┼──...┤
x.x.x| │▒▒▒▒▒▒▒│▒▒▒▒▒▒▒│  ...│
tqnqu| ├───────┼───────┼─ ...┤
mqvqj| └───────┴───────┴─ ...┘

For those who use the local mirror gitlab repository :

origin  git@NUC1:fredmj/SHELL.git (fetch)
origin  git@NUC1:fredmj/SHELL.git (push)


ok for now.


> First Written with [StackEdit](https://stackedit.io/).
> and edited with [Atom](http://www.atom.io)*(https://www.atom.io)*.
