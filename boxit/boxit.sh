#!/bin/bash
#
# According to
#
# The base of forms is
# l=┌ w=┬ k=┐		lwk 	: ┌┬┐
# t=├ n=┼ u=┤       tnu		: ├┼┤
# m=└ v=┴ j=┘       mvj		: └┴┘
#
# With length and height
#x=│ #q=─ 
#
# We obtain
#
# l7qw7qk	: ┌───────┬───────┐ 
# t7qn7qu	: ├───────┼───────┤
# m7qv7qj	: └───────┴───────┘
#
#
#Combinaison of base element with length and height give us differents Boxes type:
#
#
# Simple Box
#											  <--${cpt}-->
# simpltop	: ƒ(l,q,k) = l+${cpt}(k)+q			┌─────────────┐
# simplside	: ƒ(x,${text})= x+${text}+x			│   ${text}   │ xN
# simplbutt	: ƒ(m,q,j)= m+${cpt}(q)+j			└─────────────┘
# with ${cpt}E[1,n]
#
#
# Simple Multi Box
#
#								[${cptB}(q)+w+${cptC}(q)]
#								   ______^______
#								  /             \
# multitop : ƒ(l,w,k,q)   = l+${cptA}[${cptB}(q)+w+${cptC}(q)]+k┌────────┬────────┬───┬────────┐
# multisid : ƒ(x,${text}) = x+${textA}+x+${textB}+...+${textZ}+x│${textA}│${textB}│...│${textZ}│
# multibut : ƒ(m,v,j,q)   = m+${cptA}[${cptB}(q)+v+${cptC}(q)]+j└────────┴────────┴───┴────────┘
# with ${cpti}E[1,n] and iE[1,3]
#
#
#
#
# Simple Multi Head Box
#
# multitop : ƒ(l,w,k,q)   = l+${cptA}[${cptB}(q)+w+${cptC}(q)]+k┌────────┬────────┬───┬────────┐
# multisid : ƒ(x,${text}) = x+${textA}+x+${textB}+...+${textZ}+x│${textA}│${textB}│...│${textZ}│
# multijoi : ƒ(t,n,u)	  = t+${cptA}[${cptB}(q)+v+${cptC}(q)]+u├────────┴────────┴───┴────────┤
#
#
# Full Columns Multi Head Box
#
# multitop : ƒ(l,w,k,q)   = l+${cptA}[${cptB}(q)+w+${cptC}(q)]+k┌────────┬────────┬───┬────────┐
# multisid : ƒ(x,${text}) = x+${textA}+x+${textB}+...+${textZ}+x│${textA}│${textB}│...│${textZ}│
# multijoi : ƒ(t,n,u)	  = t+${cptA}[${cptB}(q)+n+${cptC}(q)]+u├────────┼────────┼───┼────────┤
#
# Simple Multi Bottom Box
#
# multitop : ƒ(l,w,k,q)   = t+${cptA}[${cptB}(q)+w+${cptC}(q)]+u├────────┬────────┬───┬────────┤
# multisid : ƒ(x,${text}) = x+${textA}+x+${textB}+...+${textZ}+x│${textA}│${textB}│...│${textZ}│
# multibut : ƒ(m,v,j,q)   = m+${cptA}[${cptB}(q)+v+${cptC}(q)]+j└────────┴────────┴───┴────────┘
#
#
# Full Columns Multi Bottom Box
#
# multitop : ƒ(l,w,k,q)   = t+${cptA}[${cptB}(q)+n+${cptC}(q)]+u├────────┼────────┼───┼────────┤
# multisid : ƒ(x,${text}) = x+${textA}+x+${textB}+...+${textZ}+x│${textA}│${textB}│...│${textZ}│
# multibut : ƒ(m,v,j,q)   = m+${cptA}[${cptB}(q)+v+${cptC}(q)]+j└────────┴────────┴───┴────────┘
#
#
# Simple BodyBox
#
# simple side 	:ƒ(x,${text})=x+${text}+x;			│<----------lenght------------>│
# with ${text}length=${cptB}+${cptC}+...
#
#
# Full Columns BodyBox
#
# fullmultisid: ƒ(x,${text})=x+${textA}+x+${textB}+...+${textZ}+x   │${textA}│${textB}│...│${textZ}│
#
#
#a=▒ 
#
#y=≤ #z=≥ #r=⎼ #i=↓ #o=⎺ #p=⎻ #s=⎽ #d=r #f=° #g=± #h=n #b=→ #c=ƒ 
#
#

#
init() {
export HLP_MSG="$0:Draw a square arround a text
Options are:
-t \"<text>\": the text to be squared
Note the text have to be something in quotes \"bla bla\"
in which you could insert new lines as needed"
}

Record_text() {
IFS="
"
local_cpt=0
for lines in $(echo "$1")
do
TMP_TAB[$local_cpt]="$lines"
local_cpt=$(($local_cpt+1))
done
}

Get_the_MAX_LENGTH() {
THE_MAX_LENGTH=0
local_cpt=0
for data in ${TMP_TAB[*]}
do
LINE_LENGTH=${#TMP_TAB[$local_cpt]}
[ $LINE_LENGTH -gt $THE_MAX_LENGTH ] && THE_MAX_LENGTH=$LINE_LENGTH
local_cpt=$(($local_cpt+1))
done
}

Draw_TOP_NullHeadBox () {
LOCAL_MAX_LENGTH=$THE_MAX_LENGTH
tput smacs; echo -n "l"
while [ $LOCAL_MAX_LENGTH -gt 0 ]
do
echo -n "q"
LOCAL_MAX_LENGTH=$(($LOCAL_MAX_LENGTH-1))
done
echo "k"
tput rmacs
}

Draw_CORE_NullHeadBox () {
local_cpt=0
while [ $local_cpt -lt $NB_LINES  ]
do
tput smacs; echo -n "x";tput rmacs
echo -n ${TMP_TAB[$local_cpt]}
WHITE_SPACE_LENGTH=$(($THE_MAX_LENGTH-${#TMP_TAB[$local_cpt]}))
#echo "WHITE_SPACE_LENGTH=$WHITE_SPACE_LENGTH"
#printf "%.*s" $WHITE_SPACE_LENGTH "--------------------------------------------------------------------------------"
printf "%.*s" $WHITE_SPACE_LENGTH "                                                                             "
tput smacs; echo "x";tput rmacs
local_cpt=$(($local_cpt+1))
done
}

Draw_BUTT_NullHeadBox ()
{
LOCAL_MAX_LENGTH=$THE_MAX_LENGTH
tput smacs; echo -n "m"
while [ $LOCAL_MAX_LENGTH -gt 0 ]
do
echo -n "q"
LOCAL_MAX_LENGTH=$(($LOCAL_MAX_LENGTH-1))
done
echo "j"
tput rmacs
}

Draw_Box() {
#TEXT=$1
NB_LINES=$(echo "$TEXT" | wc -l)

Record_text "$TEXT"
Get_the_MAX_LENGTH

Draw_TOP_NullHeadBox
Draw_CORE_NullHeadBox
Draw_BUTT_NullHeadBox

#
# Remind Base pattern is
#l= ┌ w= ┬ k= ┐
#t= ├ n= ┼ u= ┤
#m= └ v= ┴ j= ┘
#

# ---------------------------------------------------------------------------------------------------------------------------
#SINGLE MODE
#Draw the Null Head Box			
#
#
#lqk : ┌──────────────┐
#x x : │              │
#mqj : └──────────────┘
#



#
#The Single Head  Box	#The Single Bottom Box	#The Single Full Box
#lqk : ┌──────────────┐#lqk : ┌──────────────┐#lqk : ┌──────────────┐
#tqu : ├──────────────┤#x x : │              │#tqu : ├──────────────┤
#x x : │              │#x x : │              │#x x : │              │
#x x : │              │#tqu : ├──────────────┤#tqu : ├──────────────┤
#mqj : └──────────────┘#mqj : └──────────────┘#mqj : └──────────────┘
#
# ---------------------------------------------------------------------------------------------------------------------------
#
#MULTI MODE
# Box
#Basic Patern			#The Multi Bottom Box
#lqwqk: ┌───────┬───────┐#lqqqk	: ┌───────────────┐
#tqnqu: ├───────┼───────┤#x   x	: │               │
#m v j: └───────┴───────┘#tqwqu	: ├───────┬───────┤
#                        #mqvqj	: └───────┴───────┘
#
##The Multi Head Box	#The Full Multi Head
#lqwqk:┌───────┬───────┐#lqwqk: ┌───────┬───────┐
#tqnqu:├───────┴───────┤#tqnqu: ├───────┼───────┤
#x   x:│               │#x x x: │       │       │
#mqqqj:└───────────────┘#mqvqj: └───────┴───────┘
#
# ---------------------------------------------------------------------------------------------------------------------------
#
#The Multi Full Box
#lqwqk		: ┌───────┬───────┐
#tqnqu		: ├───────┼───────┤
#x x x		: │       │       │
#tqnqu		: ├───────┼───────┤
#mqvqj		: └───────┴───────┘
#
}

#Main
init

if [ "$#" = 0 ]; then
{
echo "You need arguments"
echo "$0 -h for help"
exit 0
}
fi

while getopts t:h options
do
case $options in
t)
TEXT="$OPTARG"
Draw_Box
;;
h)
TEXT="$HLP_MSG"
Draw_Box
exit 0
;;
?)
printf "not an option known.\n"
;;
esac
done

