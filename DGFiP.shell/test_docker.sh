#!/bin/bash

echo "{
  "insecure-registries": [
    "nexus-pull.appli.dgfip",
    "nexus-push.appli.dgfip"
  ]
}
" > /etc/docker/daemon.json


docker login -u admin -p changeme nexus-pull.appli.dgfip
docker pull nexus-pull.appli.dgfip/centos
docker build --tag httpd .
docker run -d --publish 8090:80 httpd
curl -s localhost:8090 | grep "Test Page"
docker login -u admin -p changeme nexus-push.appli.dgfip
docker tag httpd nexus-push.appli.dgfip/httpd:latest
docker push nexus-push.appli.dgfip/httpd:latest