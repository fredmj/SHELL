#!/bin/bash
###########################################################################################################
#	FMJconsulting DGFiP - 2017: Clone a remote and push to my owned gitlab project with a curl/POST command
###########################################################################################################

init() {
    BASE=/home/fredmj/Code/bricabrac/recette_baw
    PATH=$PATH:/home/fredmj/Code/bricabrac/gitlab
}

select_openstack_project(){
    select openstack in Nuage01-DGFIP-PIC
    do
    case ${openstack} in
    Nuage01-DGFIP-PIC)
    . ~/Documents/credentials/NUBO_credentials/Nuage01-DGFIP-PIC_Pic_cloud_dgfip-openrc
    break;;
    esac
    done
    for var in $(env | grep OS_); do eval export $var; done
}

set-PIC-onNUBO-withforge() {
    #------------------------------------------------------------------------------------------
    LOCAL_PARAMETERS="${BASE}/baw.parameters"
    REPO_LIST=${LOCAL_PARAMETERS}/pic/matrice-source-PIC.csv
    export PRIVATE_TOKEN=$(cat ~/Documents/credentials/GIT_credentials/PRIVATE_TOKEN.Token)
    TARGET_REMOTE_URL="https://forge.dgfip.finances.rie.gouv.fr/dgfip/soda/fmj/pic"
    NAMESPACE_ID="907"
    WORKING_DIR="${BASE}/baw.pic.dev"
    DESCRIPTION=$(echo "Virtual appliances or Ansible role providing Continous Integration platform" | sed -e's/ /\%20/g')
    BRANCH="nubo"
    MY_REMOTE="forge"
    APPLIANCE_PARAMETERS_FILE="${LOCAL_PARAMETERS}/pic/parameters-pic-nubo.yml"
    PIC_APPLIANCE_YML="${LOCAL_PARAMETERS}/pic/appliance.heat.pic.nubo.yml"
    if [ "$(git branch | grep \* | cut -d" " -f2)" == "${BRANCH}" ]; then
        echo "will install ${STACK_NAME} on ${MY_REMOTE}  from ${BRANCH}"
    else
        {
            echo "You r not on the ${BRANCH}"
            exit 1
        }
    fi
}


select_environment() {
    select your_env in PIC@forge; do
        case $your_env in
        PIC@forge)
            set-PIC-onNUBO-withforge
            break
            ;;
        esac
    done
}

download_origin() {
    export project=${1}

    REPO_NAME=$(grep "${project};" ${REPO_LIST} | cut -d";" -f1)
    REPO_URL=$(grep "${project};" ${REPO_LIST} | cut -d";" -f2)
    REPO_BRANCHE=$(grep "${project};" ${REPO_LIST} | cut -d";" -f3)

    echo "process ${project}; ${REPO_NAME}; ${REPO_URL}; ${REPO_BRANCHE}"

    echo "check ${WORKING_DIR}/${REPO_NAME}"
    [ -d ${WORKING_DIR}/${REPO_NAME} ] && {
        echo "${WORKING_DIR}/${REPO_NAME} exist; remove it before cloning"
        echo "rm -rf ${WORKING_DIR}/${REPO_NAME}"
        rm -rf ${WORKING_DIR}/${REPO_NAME}
    }

    echo "git clone ${REPO_URL} ${WORKING_DIR}/${REPO_NAME}"
    git clone ${REPO_URL} ${WORKING_DIR}/${REPO_NAME}
    echo "cd  ${WORKING_DIR}/${REPO_NAME}"
    cd ${WORKING_DIR}/${REPO_NAME}
    echo "git checkout ${REPO_BRANCHE}"
    git checkout ${REPO_BRANCHE}
}

push2Target() {
    export project=${1}
    #    echo "${BASE}/POST.gitlab \"/projects?name=${project}&namespace_id=${NAMESPACE_ID}&description=${DESCRIPTION}\""
    #    ${BASE}/POST.gitlab "/projects?name=${project}&namespace_id=${NAMESPACE_ID}&description=${DESCRIPTION}"
    echo "POST.gitlab \"/projects?name=${project}&namespace_id=${NAMESPACE_ID}&description=${DESCRIPTION}\""
    POST.gitlab "/projects?name=${project}&namespace_id=${NAMESPACE_ID}&description=${DESCRIPTION}"
    echo "git remote add ${MY_REMOTE} ${TARGET_REMOTE_URL}/${REPO_NAME}"
    git remote add ${MY_REMOTE} ${TARGET_REMOTE_URL}/${REPO_NAME}
    echo "git push ${MY_REMOTE}"
    git push ${MY_REMOTE}
    git push --tags ${MY_REMOTE}
    echo "cd ${WORKING_DIR}"
    cd ${WORKING_DIR}
}

create_project() {
    download_origin ${1}
    push2Target ${1}
}

import_all_project() {
    for project in $(cat ${REPO_LIST} | cut -d ";" -f1); do
        printf "%-37b %4b" "${project}" "[  ]"
        create_project ${project} >/dev/null 2>&1
        printf "%-37b %4b" "\r${project}" " [OK]\n"
    done
}


make_local_modification() {
    #cp ${APPLIANCE_PARAMETERS_FILE} ${WORKING_DIR}/cloud-appliance-ci/parameters.yml
    #cp ${PIC_APPLIANCE_YML} ${WORKING_DIR}/cloud-appliance-ci/appliance.heat.yml
    cp 
}

make_pic_local_changes() {
    #printf "%-37b %4b" "construct_requirement_file" "[  ]"
    #construct_requirement_file
    #printf "%-37b %4b" "\rconstruct_requirement_file" " [OK]\n"

    printf "%-37b %4b" "copy_local_modification" "[  ]"
    make_local_modification >/dev/null 2>&1
    printf "%-37b %4b" "\rcopy_local_modification" " [OK]\n"
}

create_stack() {
    if [ "$(openstack stack show -c stack_name -f value ${STACK_NAME})" == "${STACK_NAME}" ]; then
        {
            echo "openstack stack delete -y ${STACK_NAME} --wait"
            openstack stack delete -y ${STACK_NAME} --wait
        }
    fi
    while [ $(openstack stack show -c stack_name -f value "${STACK_NAME}") ]; do
        {
            echo "wait for stack ${STACK_NAME} deletion"
            sleep 2
        }
    done

    cd ${WORKING_DIR}/cloud-appliance-ci
    #echo "openstack stack create --template appliance.heat.yml -e parameters.yml ${STACK_NAME} --wait"
    #openstack stack create --template appliance.heat.yml -e parameters.yml ${STACK_NAME} --wait
    ./run.sh ${STACK_NAME}

}

set_local_etc_hosts() {
#    INSTANCE_NAME="soda"
#    URL_SUFFIXE="soda.appli.dgfip"
    INSTANCE_IP=$(openstack server show ${INSTANCE_NAME} -c addresses -f value | sed -e s'/^.*=\(.*\),.*$/\1/')
    #read -p "set INSTANCE_NAME=${INSTANCE_IP} in your /etc/hosts ?
    #(you may be invited to su as root)"
    #[ "${REPLY}" == "yes" ] && {
    for services in keycloak jenkins gitlab nexus sonarqube selenium zap; do
        sudo sh -c "sed -i -e 's/^.*${services}.${URL_SUFFIXE}.*$/${INSTANCE_IP} ${services}.${URL_SUFFIXE}/g' /etc/hosts"
    done
    #}
}

set_pic_firewall_rules() {
    #INSTANCE_NAME="soda"
    INSTANCE_IP=$(openstack server show ${INSTANCE_NAME} -c addresses -f value | sed -e s'/^.*=\(.*\),.*$/\1/')

    ssh centos@${INSTANCE_IP} "sudo sh -c \"firewall-cmd --zone=public --permanent --add-service=http\""
    ssh centos@${INSTANCE_IP} "sudo sh -c \"firewall-cmd --zone=public --permanent --add-port=7777/tcp\""
    ssh centos@${INSTANCE_IP} "sudo sh -c \"firewall-cmd --reload\""
}

set_post_local_environment() {
    printlog "set_local_etc_hosts"
    printlog "set_pic_firewall_rules"
}

create_users() {
    #INSTANCE_NAME="soda"
    INSTANCE_IP=$(openstack server show ${INSTANCE_NAME} -c addresses -f value | sed -e s'/^.*=\(.*\),.*$/\1/')
    USERS+=("Frederic MARIE-JOSEPH fredericmariejoseph@fmjconsulting.fr fredmj changeme"
        "Wenceslas PETIT wenceslas.petit@dgfip.finances.gouv.fr wpetit changeme"
        "Eric Demorsy eric.demorsy@dgfip.finances.gouv.fr eric changeme"
        "Geoffroy Wyckaert geoffroy.wyckaert@dgfip.finances.gouv.fr geoffroy changeme"
        "Xavier Chatelain xavier-l.chatelain@dgfip.finances.gouv.fr xavier changeme"
        "Mathieu Grzybek mathieu.grzybek@dgfip.finances.gouv.fr mgrzybek changeme")
    OLD_IFS=$IFS
    IFS="
"
    for users in ${USERS[@]}; do
        ssh centos@${INSTANCE_IP} "sudo sh -c \"/root/scripts/keycloak/keycloak_create_user.sh $users\""
    done
    #echo "ssh centos@${INSTANCE_IP} \"sudo sh -c \\\"/root/scripts/keycloak/keycloak_create_user.sh Frederic MARIE-JOSEPH fredericmariejoseph@fmjconsulting.fr fredmj changeme\\\"\""
    #ssh centos@${INSTANCE_IP} "sudo sh -c \"/root/scripts/keycloak/keycloak_create_user.sh Frederic MARIE-JOSEPH fredericmariejoseph@fmjconsulting.fr fredmj changeme\""
    IFS=$OLD_IFS
}


init
select_openstack_project
select_environment
#set_credentials_in_parameters

select action in import-one-project import-all-project pic_local_changes create-stacks install-all-pic set_post_local_environment get_log create_users debug; do
    case $action in
    import-one-project)
        select project in $(cat ${REPO_LIST} | cut -d ";" -f1); do
            echo "create_project ${project}"
            create_project ${project}
            break
        done
        break
        ;;
    import-all-project)
        import_all_project
        break
        ;;
    pic_local_changes)
        make_pic_local_changes
        break
        ;;
    create-stacks)
        create_stack
        break
        ;;
    install-all-pic)
        import_all_project
        make_pic_local_changes
        create_stack
        break
        ;;
    set_post_local_environment)
        set_post_local_environment
        break
        ;;
    get_log)
        get_log
        break
        ;;
    create_users)
        create_users
        break
        ;;
    debug)
        echo "debug"
        break
        ;;
    esac
    break
done
