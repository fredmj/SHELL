#!/bin/bash
# FMJ2020 - DGFiP
# ----
exec 2>&- 
init() {
    [ -f ~/.npmrc ] && rm ~/.npmrc
    [ -d ~/.npm/ ] && rm -rf ~/.npm
    [ -d myapp/ ] && rm -rf myapp
    [ -d myapp2/ ] && rm -rf myapp2
    PROXY_REGISTRY="http://nexus-cloud.appli.dgfip/repository/npm_dgfip/"
    PUBLISH_REGISTRY="http://nexus-cloud.appli-preprod.dgfip/repository/hosted-npm/"
    WORKING_DIR=$(pwd)

    LOG_FILE="test-$(date +%Y-%m-%d).log"
    RES_FILE="test-$(date +%Y-%m-%d).txt"
    echo "TEST-RUN-NODEJS@$(date +%H:%M:%S)" >> $LOG_FILE

    npm config set registry ${PROXY_REGISTRY}
    npm config set _auth YWRtaW46Y2hhbmdlbWU=
}

 init
 mkdir myapp
 cp local.app.js myapp/app.js
 
install_expressjs() {
 cd ${WORKING_DIR}/myapp
 npm init -y
 npm install express --no-save
 npm publish --registry=${PUBLISH_REGISTRY}
 cd -
 }

pull_n_push() {
 npm search --registry=${PUBLISH_REGISTRY} myapp
 mkdir myapp2
 cd myapp2
 npm install --registry=${PUBLISH_REGISTRY} myapp
 cd -
}
    for action in install_expressjs pull_n_push
    do
    printf "%-37b %4b" "${action}" "[  ]"
    ${action} >> ${LOG_FILE}
    if [ $? -eq '0' ]
       then printf "%-37b %4b" "\r${action} (RC:$?)" " [OK]\n" && printf "%-37b %4b" "${action} (RC:$?)" " [OK]\n" >> ${RES_FILE}
       else printf "%-37b %4b" "\r${action} (RC:$?)" " [KO]\n" && printf "%-37b %4b" "${action} (RC:$?)" " [OK]\n" >> ${RES_FILE}
    fi
    done
