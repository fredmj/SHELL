#!/bin/bash
# FMJ DGFiP - 2020
# 1- get ip;localhost of NEXUS-COMPOSER & PIC from /etc/hosts
# 2- get all results*.txt and math the sum
# 3- create html page and pdf report


init() {
    DATE=$(date +%Y-%m-%dT%H-%M-%S)
    BASE="/home/fredmj/Code/bricabrac/recette_baw"
    . /home/fredmj/credentials/OVH/mKt6/auto-3574584998823461-v3-openrc.sh
    SCORE_FILE="${BASE}/nexus_${DATE}_$$.csv"
}

run_tests() {
    # remove old log and txt
    rm ${SCORE_FILE}
    find . -name "test-2020*.log" -exec rm \{} \;
    find . -name "test-2020*.txt" -exec rm \{} \;

    #run nexus tests : campagne
    cd ${BASE}/nexus;
    ./recette_nexus.sh <<- EOF
    2
EOF
    #run nexus tests : create blob
    cd ${BASE}/nexus;
    ./recette_nexus.sh <<-EOF
    1
    1
EOF
    #run nexus tests : create repo
    cd ${BASE}/nexus;
    ./recette_nexus.sh <<-EOF
    1
    3
EOF
    #run maven
    cd ${BASE}/maven;
    ./test_maven.sh

    #rum nodejs
    cd ${BASE}/nodejs;
    ./test_nodejs.sh

    #run yum
    cd ${BASE}/yum;
    ./test_yum.sh

    #run nexus tests : backup-delete-restore
#    cd ${BASE}/nexus;
#    ./recette_nexus.sh <<-EOF
#    6
#EOF

}

get_tests_results() {
    #find . -name "test*.txt" -exec cat \{} \; | sed -e's/ *//g' | sed -e's/(RC:.*)/;/g'
    find ${BASE} -name "test*.txt" -exec cat \{} \; | sed -e's/ *//g' | sed -e's/(RC:.*)/;/g' > ${SCORE_FILE}
}

make_the_score(){
#  x= (100.TOTAL_TESTS)/TOTAL_OK

    TOTAL_TESTS=$(wc -l ${SCORE_FILE} | cut -d' ' -f1 )
    TOTAL_OK=$(grep OK ${SCORE_FILE} | wc -l | cut -d' ' -f1)
    SCORE=$(echo "(${TOTAL_OK}/${TOTAL_TESTS})*100" | bc -l)
    #SCORE=$(((${TOTAL_OK}/${TOTAL_TESTS})*100))

}

make_report() {
    echo "TOTAL_TESTS=${TOTAL_TESTS}"
    echo "TOTAL_OK=${TOTAL_OK}"
    printf "SCORE=%.5s" "${SCORE}"
    echo %
}

init
run_tests
get_tests_results
make_the_score
make_report