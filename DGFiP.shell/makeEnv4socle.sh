#!/bin/bash
# Make the env.tfvars
cat > terraform.tfvars << EOF
cloud_front_office_net_id = "$(openstack network list | awk '/PUBLICATION/ {print $2}')"
cloud_back_office_net_id = "$(openstack network list | awk '/ADMINISTRATION/ {print $2}')"
front_dns_nameservers = ["10.154.59.104"]
EOF
