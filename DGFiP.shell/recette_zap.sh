#!/bin/bash
# FMJ 2020 - DGFiP

API_KEY=ZAPROXYPLUGIN
#ZAP_HOST="zap.soda.appli.dgfip"
ZAP_HOST="localhost"
ZAP_PORT="7777"
TARGET="juiceshop.appli.dgfip"

LOG_FILE="test-$(date +%Y-%m-%d).log"
RES_FILE="test-$(date +%Y-%m-%d).txt"
echo "TEST-RUN-ZAP@$(date +%H:%M:%S)" >> $LOG_FILE
echo "API_KEY=${API_KEY}" >> $LOG_FILE
#echo "JENKINS_MODE=${JENKINS_MODE}" >> $LOG_FILE

make_spider_scan() {
 # crawl a spider - result is something like {"scan":"0"}
 SCAN_ID=$(curl -s "http://${ZAP_HOST}:7777/JSON/spider/action/scan/?apikey=${API_KEY}&url=http://juiceshop.appli.dgfip&contextName=&recurse=" | jq -Mr '.scan')
 echo "SCAN_ID=${SCAN_ID}"
 echo
}

wait_spider_scan() {
 # wait while it work
 #echo curl -s \"http://localhost:7777/JSON/spider/view/status/?apikey=${API_KEY}\&scanId=${SCAN_ID}\ | jq .status"
  while [ $(curl -s "http://${ZAP_HOST}:7777/JSON/spider/view/status/?apikey=${API_KEY}&scanId=${SCAN_ID}" | jq -Mr '.status') != "100" ]
  do
      echo "wait for ${SCAN_ID}"
      curl "http://${ZAP_HOST}:7777/JSON/spider/view/status/?apikey=${API_KEY}&scanId=${SCAN_ID}"
  done
 echo
}

show_spider_result() {
 # crawl result
 echo "CRAWL RESULT"
 curl -s "http://${ZAP_HOST}:7777/JSON/spider/view/results/?apikey=${API_KEY}&scanId=${SCAN_ID}"
 echo
}

make_ajax_spider() {
     # Ajax spider
 curl -s "http://${ZAP_HOST}:7777/JSON/ajaxSpider/action/scan/?apikey=${API_KEY}&url=http://juiceshop.appli.dgfip&inScope=&contextName=&subtreeOnly="
 echo
}

wait_ajax_spider() {
    # View status
  while [ $(curl -s "http://${ZAP_HOST}:7777/JSON/ajaxSpider/view/status/?apikey=${API_KEY}" | jq -Mr '.status') == "running" ]
  do
      STATUS=$(curl -s "http://${ZAP_HOST}:7777/JSON/ajaxSpider/view/status/?apikey=${API_KEY}" | jq -Mr '.status')
      printf "%-37b %4b" "\rstatus (RC:$?)" " [${STATUS}]\n"
  done
 echo
}

show_number_result_ajax() {
 # To view the number of results
 curl -s "http://${ZAP_HOST}:7777/JSON/ajaxSpider/view/numberOfResults/?apikey=${API_KEY}"
 echo
}

view_result_ajax() {
    # To view the results
 curl -s "http://${ZAP_HOST}:7777/JSON/ajaxSpider/view/fullResults/?apikey=${API_KEY}"
 echo
}

make_active_scan1() {
    # Active scan
 echo "ACTIVE SCAN TYPE1"
 curl -s "http://${ZAP_HOST}:7777/JSON/ascan/action/scan/?apikey=${API_KEY}&url=http://juiceshop.appli.dgfip&recurse=true&inScopeOnly=&scanPolicyName=&method=&postData=&contextId="
 echo
}

start_active_scan() { 
    echo "ACTIVE SCAN TYPE2"
    SCAN_URL="http://$1:$2/"
    SCAN_URL+="JSON/ascan/action/scan/?"
    SCAN_URL+="zapapiformat=JSON&"
    SCAN_URL+="formMethod=GET&"
    SCAN_URL+="url=https://$3&"
     
    # Start Active ZAP Scan
    SCAN_ID_RES=$(curl -s $SCAN_URL)
    # Parse for scan ID
    SCAN_ID=$(echo $SCAN_ID_RES | jq -r '.scan')
    # Display scan ID
    echo Scan ID: $SCAN_ID 
}

make_active_scan2() {
    start_active_scan $ZAP_HOST $ZAP_PORT $TARGET 
}

    for action in make_spider_scan wait_spider_scan show_spider_result make_ajax_spider wait_ajax_spider show_number_result_ajax view_result_ajax make_active_scan1 make_active_scan2
    do
    printf "%-37b %4b" "${action}" "[  ]"
    ${action} >> ${LOG_FILE}
    if [ $? -eq '0' ]
       then printf "%-37b %4b" "\r${action} (RC:$?)" " [OK]\n" && printf "%-37b %4b" "${action} (RC:$?)" " [OK]\n" >> ${RES_FILE}
       else printf "%-37b %4b" "\r${action} (RC:$?)" " [KO]\n" && printf "%-37b %4b" "${action} (RC:$?)" " [OK]\n" >> ${RES_FILE}
    fi
    done