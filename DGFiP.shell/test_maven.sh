#!/bin/bash
# FMJ 2020 - DGFiP


export PATH=$PATH:/opt/maven/apache-maven-3.6.2/bin

init() {
  [ -d test ] && (rm -rf test/)
  [ -f local.settings.xml ] && (rm -f local.settings.xml)

  URL_NEXUS="http://nexus-cloud.appli-preprod.dgfip/repository"
  LOG_FILE="test-$(date +%Y-%m-%d).log"
  RES_FILE="test-$(date +%Y-%m-%d).txt"
  echo "TEST-RUN-MAVEN@$(date +%H:%M:%S)" >> $LOG_FILE

  # by default
  #PROXY_REPO_ID="maven_public"
  #PATH_REPO_PROXY="maven_public"

  #SNAPSHOT_REPO_ID="maven-snapshot"
  #PATH_REPO_SNAPSHOT="maven-snapshot"

  #RELEASE_REPO_ID="maven_public"
  #PATH_REPO_RELEASE="maven_public"

  # surcharge local conf
  source ./parameters/repo.conf

  BUILD_VERSION=1
}

create_local_settings() {
  # create a settings.xmlfit configuration to your need
  echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
 <settings xmlns=\"http://maven.apache.org/SETTINGS/1.1.0\"
  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
  xsi:schemaLocation=\"http://maven.apache.org/SETTINGS/1.1.0 http://maven.apache.org/xsd/settings-1.1.0.xsd\">

<pluginGroups>
    <pluginGroup>org.sonarsource.scanner.maven</pluginGroup>
</pluginGroups>
<profiles>
    <profile>
        <id>sonar</id>
        <activation>
            <activeByDefault>true</activeByDefault>
        </activation>
        <properties>
            <!-- Optional URL to server. Default value is http://localhost:9000 -->
            <sonar.host.url>
              ${URL_SONARQUBE}
            </sonar.host.url>
        </properties>
    </profile>
 </profiles>

 <servers>
   <server>
     <id>${PROXY_REPO_ID}</id>
     <username>admin</username>
     <password>changeme</password>
   </server>
   <server>
    <id>${SNAPSHOT_REPO_ID}</id>
    <username>admin</username>
    <password>changeme</password>
   </server>
   <server>
    <id>${RELEASE_REPO_ID}</id>
    <username>admin</username>
    <password>changeme</password>
   </server>
 </servers>
 <mirrors>
  <mirror>
    <id>${PROXY_REPO_ID}</id>
    <name>${PROXY_REPO_ID}</name>
   <url>${URL_NEXUS}/${PROXY_REPO_ID}</url>
    <mirrorOf>*</mirrorOf>
  </mirror>
 </mirrors>
 </settings>" > local.settings.xml
}

copy_local_settings() {
  cp ./parameters/settings.xml local.settings.xml
}

create_maven_test_directory() {
 # start from scratch
 rm -rf ~/.m2
 mvn -q -s local.settings.xml archetype:generate \
 -DgroupId=localnexus.dgfip.test \
 -DartifactId=test \
 -DarchetypeArtifactId=maven-archetype-quickstart \
 -DarchetypeVersion=1.4 \
 -DinteractiveMode=false
}

change_pom() {
 #sed '/PATTERN/ i <LINE-TO-BE-ADDED\>' FILE.txt
 sed -i -e "/<build/ i       <repositories\>\n\
 <!-- Publish the versioned releases here --\>\n\
      <repository\>\n\
       <id\>${PROXY_REPO_ID}</id\>\n\
       <name\>${PROXY_REPO_ID}</name\>\n\
       <url\>${URL_NEXUS}/${PATH_REPO_PROXY}</url\>\n\
      </repository\>\n\
    </repositories\>\n\
  \n\
     <distributionManagement\>\n\
      <!-- Publish the versioned releases here --\>\n\
       <repository\>\n\
        <id\>${RELEASE_REPO_ID}</id\>\n\
        <name\>${RELEASE_REPO_ID}</name\>\n\
        <url\>${URL_NEXUS}/${PATH_REPO_RELEASE}</url\>\n\
       </repository\>\n\
      <snapshotRepository\>\n\
        <id\>${SNAPSHOT_REPO_ID}</id\>\n\
        <url\>${URL_NEXUS_SNAP}/${PATH_REPO_SNAPSHOT}</url\>\n\
      </snapshotRepository\>\n</distributionManagement\>" test/pom.xml
}

build_project() {
  mvn -q -s local.settings.xml -f test/pom.xml clean compile
  mvn -q -s local.settings.xml -f test/pom.xml clean test
  mvn -q -s local.settings.xml -f test/pom.xml clean package
}

get_current_version() {
  CURRENT_VERSION=$(mvn -s local.settings.xml \
  -f test/pom.xml \
  org.apache.maven.plugins:maven-help-plugin:3.1.0:evaluate \
  -Dexpression=project.version -q -DforceStdout)
}

make_a_change_in_project() {
  sed -i -e '11 a \\tSystem.out.println( "Change@$(date +%H:%M:%s)" );' test/src/main/java/localnexus/dgfip/test/App.java
  BUILD_VERSION=$(($BUILD_VERSION+1))
}

update_to_snapshot_version() {
  mvn -q -s local.settings.xml -f test/pom.xml versions:set -DnewVersion=1.0.${BUILD_VERSION}-SNAPSHOT
}

call_sonar() {
  mvn -q -s local.settings.xml -f test/pom.xml sonar:sonar
}

deploy() {
  mvn -q -s local.settings.xml -f test/pom.xml clean deploy
}

update_to_release_version() {
  mvn -q -s local.settings.xml -f test/pom.xml versions:set -DnewVersion=1.0.${BUILD_VERSION}
}

create_snapshot_repo() {
  CURLIT="curl -s -u admin:changeme -X GET http://nexus.fmj.appli.dgfip/service/rest/v1/repositories"
  repo_name=$(grep repositoryName ${PARAME_REPO_SNAP} | cut -d":" -f2 | sed -e's/\"//g')

  # If repo doesn't already exist...
  [ -z $(${CURLIT} | jq .[].name | grep $repo_name ) ] && {
    # will create "maven_snapshot" snapshot repo
    cd ${SCRIPT_REPO}
    ${SCRIPT_REPO}/create_repository.sh ${PARAME_REPO_SNAP}
    cd -
  }
}

create_project() {
    echo "[${0}:${FUNCNAME}]" >> $LOG_FILE
    for actions in create_local_settings create_maven_test_directory change_pom build_project get_current_version
      do
      ${actions} >> ${LOG_FILE}
    done
}

deploy_snapshot() {
  #create_snapshot_repo
    echo "[${0}:${FUNCNAME}]" >> $LOG_FILE
    for actions in make_a_change_in_project get_current_version update_to_snapshot_version get_current_version deploy
      do
      ${actions} >> ${LOG_FILE}
      if [ $? -eq '0' ]
         then printf "%-37b %4b" "${actions} (RC:$?)" " [OK]\n" >> $RES_FILE
         else printf "%-37b %4b" "${actions} (RC:$?)" " [KO]\n" >> $RES_FILE
      fi
    done
}

deploy_release() {
  # RELEASE
    echo "[${0}:${FUNCNAME}]" >> $LOG_FILE
    for actions in make_a_change_in_project get_current_version update_to_release_version get_current_version deploy
      do
      ${actions} >> ${LOG_FILE}
      if [ $? -eq '0' ]
         then printf "%-37b %4b" "${actions} (RC:$?)" " [OK]\n" >> $RES_FILE
         else printf "%-37b %4b" "${actions} (RC:$?)" " [KO]\n" >> $RES_FILE
      fi
    done
}

init
  echo "URL_NEXUS=${URL_NEXUS}"
  echo "URL_NEXUS_SNAP=${URL_NEXUS_SNAP}"
  echo "PROXY_REPO_ID=${PROXY_REPO_ID}"
  echo "PATH_REPO_PROXY=${PATH_REPO_PROXY}"
  echo "SNAPSHOT_REPO_ID=${SNAPSHOT_REPO_ID}"
  echo "PATH_REPO_SNAPSHOT=${PATH_REPO_SNAPSHOT}"
  echo "RELEASE_REPO_ID=${RELEASE_REPO_ID}"
  echo "PATH_REPO_RELEASE=${PATH_REPO_RELEASE}"

    for action in create_project deploy_snapshot call_sonar deploy_release
    do
    printf "%-37b %4b" "${action}" "[  ]"
    ${action} >> ${LOG_FILE}
    if [ $? -eq '0' ]
       then printf "%-37b %4b" "\r${action} (RC:$?)" " [OK]\n"
       else printf "%-37b %4b" "\r${action} (RC:$?)" " [KO]\n"
    fi
    done
