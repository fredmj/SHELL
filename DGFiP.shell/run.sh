#!/usr/bin/env bash

# Get parameters
. inc/parse_yaml.sh
PARAMETERS_FILE=${1:-parameters.yml}

if [ ! -f $PARAMETERS_FILE ]; then
    echo "Vous devez renseigner le fichier de paramètres"
    exit 1
fi
eval $(parse_yaml $PARAMETERS_FILE "p_")

NEXUS_CHECKOUT=${p_parameters_nexus_branche}
SATIS_CHECKOUT=${p_parameters_composer_branche}
PROXY_CHECKOUT=${p_parameters_proxy_branche}
SCRIPTS_CHECKOUT=${p_parameters_scriptsNexus_branche}

# If $version_tag is not empty get versions array in file and change clone checkout
if [ ! -z "${p_parameters_version_tag}" ]; then
  version_arr=( $(jq -r --arg ver "${p_parameters_version_tag}" '.[$ver][]' .versions.json) )

  if [ $? -ne 0 ];then
    echo "Le version tag ne semble pas correct"
    exit 1
  fi

  NEXUS_CHECKOUT=${version_arr[0]}
  SATIS_CHECKOUT=${version_arr[1]}
  PROXY_CHECKOUT=${version_arr[2]}
  SCRIPTS_CHECKOUT=${version_arr[3]}
fi

# Clone Nexus Repository

cat <<EOF

**************************************************************************
********************* Récupération des repositories **********************
**************************************************************************

EOF

echo "Clone Nexus Repository from ${NEXUS_CHECKOUT} in ${p_parameters_nexus_directory}"
rm -rf ${p_parameters_nexus_directory}
if [ ! -z "${p_parameters_git_username}${p_parameters_git_password}" ] ; then
 auth_git_repo_url=$(echo ${p_parameters_nexus_repository} | awk -F// -v user=${p_parameters_git_username}  -v password=${p_parameters_git_password} '{print $1"//"user":"password"@"$2}')
  git clone -q -b ${NEXUS_CHECKOUT} ${auth_git_repo_url} ${p_parameters_nexus_directory} > /dev/null 2>&1 || exit 1
else
	git clone -q -b ${NEXUS_CHECKOUT} ${p_parameters_nexus_repository} ${p_parameters_nexus_directory} > /dev/null 2>&1 || exit 1
fi

cp "${p_parameters_nexus_directory}/parameters.yml.default" "${p_parameters_nexus_directory}/parameters.yml"
 
# Clone Composer Repository
echo "Clone Composer Repository from ${SATIS_CHECKOUT} in ${p_parameters_composer_directory}"
rm -rf ${p_parameters_composer_directory}
if [ ! -z "${p_parameters_git_username}${p_parameters_git_password}" ] ; then
 auth_git_composer_url=$(echo ${p_parameters_composer_repository} | awk -F// -v user=${p_parameters_git_username}  -v password=${p_parameters_git_password} '{print $1"//"user":"password"@"$2}')
  git clone -q -b ${SATIS_CHECKOUT} ${auth_git_composer_url} ${p_parameters_composer_directory} > /dev/null 2>&1 || exit 1
else
	git clone -q -b ${SATIS_CHECKOUT} ${p_parameters_composer_repository} ${p_parameters_composer_directory} > /dev/null 2>&1 || exit 1
fi

cp "${p_parameters_composer_directory}/parameters.yml.default" "${p_parameters_composer_directory}/parameters.yml"

# Clone Proxy Repository
echo "Clone Proxy Repository from ${PROXY_CHECKOUT} in ${p_parameters_proxy_directory}"
rm -rf ${p_parameters_proxy_directory}
if [ ! -z "${p_parameters_git_username}${p_parameters_git_password}" ] ; then
 auth_git_proxy_url=$(echo ${p_parameters_proxy_repository} | awk -F// -v user=${p_parameters_git_username}  -v password=${p_parameters_git_password} '{print $1"//"user":"password"@"$2}')
  git clone -q -b ${PROXY_CHECKOUT} ${auth_git_proxy_url} ${p_parameters_proxy_directory} > /dev/null 2>&1 || exit 1
else
	git clone -q -b ${PROXY_CHECKOUT} ${p_parameters_proxy_repository} ${p_parameters_proxy_directory} > /dev/null 2>&1 || exit 1
fi

cp "${p_parameters_proxy_directory}/parameters.yml.default" "${p_parameters_proxy_directory}/parameters.yml"

# Clone Nexus Scripts Repository
echo "Clone Nexus Scripts Repository from ${SCRIPTS_CHECKOUT} in ${p_parameters_scriptsNexus_directory}"
rm -rf ${p_parameters_scriptsNexus_directory}
if [ ! -z "${p_parameters_git_username}${p_parameters_git_password}" ] ; then
 auth_git_scriptsNexus_url=$(echo ${p_parameters_scriptsNexus_repository} | awk -F// -v user=${p_parameters_git_username}  -v password=${p_parameters_git_password} '{print $1"//"user":"password"@"$2}')
 git clone -q -b ${SCRIPTS_CHECKOUT} ${auth_git_scriptsNexus_url} ${p_parameters_scriptsNexus_directory} > /dev/null 2>&1 || exit 1
else
	git clone -q -b ${SCRIPTS_CHECKOUT} ${p_parameters_scriptsNexus_repository} ${p_parameters_scriptsNexus_directory} > /dev/null 2>&1 || exit 1
fi

cat <<EOF

**************************************************************************
*********************    Création des appliances    **********************
**************************************************************************

EOF

### Fill Nexus parameters
. inc/set_nexus_parameters.sh

### Fill Composer parameters
. inc/set_composer_parameters.sh

### Fill Proxy parameters
. inc/set_proxy_parameters.sh

composer_parameter_file="${p_parameters_composer_directory}/parameters.yml"
proxy_parameter_file="${p_parameters_proxy_directory}/parameters.yml"

# Ensure satis_domain is correct
# Remove satis_domain from proxy parameter file
sed -i "/^  satis_domain:/d" ${composer_parameter_file}

# Set new virtual hosts in proxy parameter file
echo '  satis_domain: "'${p_parameters_composer_domain}'"' >> ${composer_parameter_file}

. inc/nexus.sh
. inc/composer.sh

# Set Static_hosts
STATIC_HOSTS='["localnexus":"'${nexus_floatingIp}'","drupalpackagist":"'${composer_floatingIp}'"]'
sed -i 's~\ \ static_hosts:.*~\ \ static_hosts: '${STATIC_HOSTS}'~g' "${proxy_parameter_file}"

# Set new virtual hosts in proxy parameter file
VIRTUAL_HOSTS='[{"name":"'${p_parameters_nexus_domain}'","dest":"http://localnexus:8081/"},{"name":"'${p_parameters_nexus_domain_docker_pull}'","dest":"http://localnexus:8082/"},{"name":"'${p_parameters_nexus_domain_docker_push}'","dest":"http://localnexus:8083/"},{"name":"'${p_parameters_composer_domain}'","dest":"http://drupalpackagist/"}]'
sed -i 's~\ \ virtual_hosts:.*~\ \ virtual_hosts: '${VIRTUAL_HOSTS}'~g' "${proxy_parameter_file}"

. inc/proxy.sh
