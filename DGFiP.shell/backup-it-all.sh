#!/bin/bash

#Ansible: Keycloak backup
/root/scripts/keycloak/keycloak_create_backup.sh > /tmp/keycloak_backup.log 2>&1

#Ansible: Jenkins backup
/root/scripts/jenkins/jenkins_create_backup.sh > /tmp/jenkins_backup.log 2>&1

#Ansible: Gitlab backup
/root/scripts/gitlab/gitlab_create_backup.sh > /tmp/gitlab_backup.log 2>&1

#Ansible: Sonarqube backup
/root/scripts/sonarqube/sonarqube_create_backup.sh > /tmp/sonarqube_backup.log 2>&1

#Ansible: Backup volume: fmj_keycloak
/root/backup_script/backup.sh b23ef547-cd70-48ea-b2db-020a01b47e02 /root/backup_script fmj_keycloak > /tmp/fmj_keycloak.log 2>&1
sleep 60

#Ansible: Backup volume: fmj_jenkins
/root/backup_script/backup.sh 76e26b04-c457-4e7b-9a32-2430d47b2807 /root/backup_script fmj_jenkins > /tmp/fmj_jenkins.log 2>&1
sleep 60

#Ansible: Backup volume: fmj_nexus
/root/backup_script/backup.sh 88b02dbf-d3f0-4904-ad04-b38bdfb28bc2 /root/backup_script fmj_nexus > /tmp/fmj_nexus.log 2>&1
sleep 60

#Ansible: Backup volume: fmj_gitlab
/root/backup_script/backup.sh 5bb57823-0b11-47f8-9b21-ca039d71c091 /root/backup_script fmj_gitlab > /tmp/fmj_gitlab.log 2>&1
sleep 60

#Ansible: Backup volume: fmj_sonarqube
/root/backup_script/backup.sh 28ee7c50-f985-4f42-9386-7f24e26d04f4 /root/backup_script fmj_sonarqube > /tmp/fmj_sonarqube.log 2>&1
sleep 60
