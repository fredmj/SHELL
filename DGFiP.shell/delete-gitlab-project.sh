#!/bin/bash
################################################################################
#
#	FMJ: 2017: DELETE gitlab project(s)  with a curl/POST API command
################################################################################
# curl -X DELETE  --header "Private-Token: ${PRIVATE_TOKEN}" ${GITLAB_URL}/api/v4/
#./GET.gitlab /project/:id
#./GET.gitlab /projects?owned=true | jq '.[].id'
# GROUP_ID=group of a project
# Group ID: 545 = NEXUS_SATIS
# Group ID: 546 = PIC
# Group ID: 7424613 - NEXUS-SATIS on gitlab.com
# Group ID: 7424617 - PIC on gitlab.com


# GET /groups/:id/projects : List a group’s projects.

set_environment(){
    select your_env in NEXUS-SATIS@forge PIC@forge NEXUS-SATIS@GITLAB.COM PIC@GITLAB.COM
    do
    case $your_env in
        NEXUS-SATIS@forge)
        echo "set-NEXUS-SATIS-onNUBO-withforge"
        break;;
        PIC@forge)
        echo "set-NEXUS-SATIS-onNUBO-withforge"
        break;;
        NEXUS-SATIS@GITLAB.COM)
        TMA_GROUP_ID=7424613
		echo "will erase NEXUS-SATIS on gitlab.com"
        break;;
        PIC@GITLAB.COM)
        TMA_GROUP_ID=7424617
		echo "will erase PIC on gitlab.com"
        break;;
    esac
    done
}

set_environment

select action in one-project all-myproject all-TMA-project
do
	case $action in
		one-project)
			select project in $(./GET.gitlab /projects?owned=true | jq -r '.[] | [.id, .name] | @tsv' | sed -e 's/[[:cntrl:]]/;/g')
			do
				./DELETE.gitlab /projects/${project%%;*}
				break
			done
		break
		;;
		all-myproject)
			for id in $(./GET.gitlab /projects?owned=true | jq '.[].id')
			do
				echo "./DELETE.gitlab /project/${id}"
			done
		break;;
		all-TMA-project)
		read -p "will erase all TMA project as in group #${TMA_GROUP_ID} (yes/no)?"
		if [ ${REPLY} == "yes" ]; then
			{
			for id in $(./GET.gitlab /groups/${TMA_GROUP_ID}/projects | jq '.[].id')
			do
				echo "./DELETE.gitlab /projects/${id}"
				eval ./DELETE.gitlab /projects/${id}
			done
			}
		else 
			{
				echo "You have to type 'yes', all other answer will be assumed as a 'no'."
				exit 0
			}
		fi
		break;;
	esac
	break
done