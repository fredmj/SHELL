#!/bin/bash

yum install -y maven
curl -sL https://rpm.nodesource.com/setup_13.x | bash -
yum install -y nodejs
yum install -y php
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php composer-setup.php --install-dir=/usr/bin --filename=composer
php -r "unlink('composer-setup.php');"
