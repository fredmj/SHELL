#!/bin/bash
###########################################################################################################
#	FMJconsulting DGFiP - 2017: Clone a remote and push to my owned gitlab project with a curl/POST command
###########################################################################################################

init() {
    BASE=/home/fredmj/Code/bricabrac/recette_baw
    PATH=$PATH:/home/fredmj/Code/bricabrac/gitlab
    [ ! -d logs ] && mkdir logs
    LOG_DIR="logs"
}

[ ${WORKSPACE} ] && {
        JENKINS_MODE=1
	    TEST_REPO="${WORKSPACE}/recette_baw/nexus"
	} || { 
		JENKINS_MODE=0
        TEST_REPO="/home/fredmj/Code/bricabrac/recette_baw/nexus"
	}
echo "JENKINS_MODE=${JENKINS_MODE}"


set-NEXUS-SATIS-onNUBO-withforge() {
 #------------------------------------------------------------------------------------------
. ~/Documents/credentials/NUBO_credentials/Nuage01-DGFIP-DMOD_forge_ice_dev-openrc
 LOCAL_PARAMETERS="${BASE}/baw.parameters/nexus-satis"
 LOCAL_NEXUS_SATIS_REQ="${LOCAL_PARAMETERS}/parameters-nexus-satis-nubo.yml"
 REPO_LIST=${LOCAL_PARAMETERS}/matrice-source-NEXUS-COMPOSER.csv
 export PRIVATE_TOKEN=$(cat ~/Documents/credentials/GIT_credentials/PRIVATE_TOKEN.Token)
 #TARGET_REMOTE_URL="https://oauth2:${PRIVATE_TOKEN}@forge.dgfip.finances.rie.gouv.fr/dgfip/soda/cloud/fmj/nexus_satis"
 TARGET_REMOTE_URL=https://forge.dgfip.finances.rie.gouv.fr/dgfip/soda/fmj/nexus_satis
 NAMESPACE_ID="658"
 WORKING_DIR="${BASE}/baw.nexus_satis.preprod"
 DESCRIPTION=$(echo "Virtual appliances or Ansible role providing Repository Management services" | sed -e's/ /\%20/g')
 MY_REMOTE="forge"
 #RUN="run.sh"
 echo "NAMESPACE_ID=${NAMESPACE_ID}"

 }

set_environment(){
    select your_env in NEXUS-SATIS@forge
    do
    case $your_env in
        NEXUS-SATIS@forge)
        set-NEXUS-SATIS-onNUBO-withforge
        break;;
    esac
    done
 }

download_origin() {
    export project=${1}

    REPO_NAME=$(grep "${project};" ${REPO_LIST} | cut -d";" -f1)
    REPO_URL=$(grep "${project};" ${REPO_LIST} | cut -d";" -f2)
    REPO_BRANCHE=$(grep "${project};" ${REPO_LIST} | cut -d";" -f3)

    echo "process ${project}; ${REPO_NAME}; ${REPO_URL}; ${REPO_BRANCHE}"

    echo "check ${WORKING_DIR}/${REPO_NAME}"
    [ -d ${WORKING_DIR}/${REPO_NAME} ] && {
        echo "${WORKING_DIR}/${REPO_NAME} exist; remove it before cloning"
        echo "rm -rf ${WORKING_DIR}/${REPO_NAME}"
        rm -rf ${WORKING_DIR}/${REPO_NAME}
    }

    echo "git clone ${REPO_URL} ${WORKING_DIR}/${REPO_NAME}"
    git clone ${REPO_URL} ${WORKING_DIR}/${REPO_NAME}
    echo "cd  ${WORKING_DIR}/${REPO_NAME}"
    cd  ${WORKING_DIR}/${REPO_NAME}
    echo "git checkout ${REPO_BRANCHE}"
    git checkout ${REPO_BRANCHE}
 }

push2Target() {
    export project=${1}
    echo "POST.gitlab \"/projects?name=${project}&namespace_id=${NAMESPACE_ID}&description=${DESCRIPTION}\""
    POST.gitlab "/projects?name=${project}&namespace_id=${NAMESPACE_ID}&description=${DESCRIPTION}"
    echo "git remote add ${MY_REMOTE} ${TARGET_REMOTE_URL}/${REPO_NAME}"
    git remote add ${MY_REMOTE} ${TARGET_REMOTE_URL}/${REPO_NAME}
    echo "git push ${MY_REMOTE}"
    git push ${MY_REMOTE}
    git push --tags ${MY_REMOTE}
    echo "cd ${WORKING_DIR}"
    cd ${WORKING_DIR}
 }

create_project() {
    download_origin ${1}
    push2Target ${1}
 }

import_all_projects() {
    	for project in $(cat ${REPO_LIST} | cut -d ";" -f1)
			do
                printf "%-37b %4b" "${project}" "[  ]"
                create_project ${project} > /dev/null 2>&1
                printf "%-37b %4b" "\r${project}" " [OK]\n"
            done
 }

copy_local_modification() {
    # Copy the predefined parameter file into main dir for prerequisites.sh
    cp ${LOCAL_NEXUS_SATIS_REQ} ${WORKING_DIR}/nexus_satis_servers/parameters.yml
    #cd ${WORKING_DIR}/nexus_satis_servers

    # Copy the 3 owned appliances template files into appliance dir
    #cp ${LOCAL_PARAMETERS}/appliance.heat.nexus4ovh.yml ${WORKING_DIR}/cloud-appliance-nexus/appliance.heat.yml
    cp ${LOCAL_PARAMETERS}/appliance.heat.composer.nubo.yml ${WORKING_DIR}/cloud-appliance-composer/appliance.heat.yml
    #cp ${LOCAL_PARAMETERS}/appliance.heat.proxy4ovh.yml ${WORKING_DIR}/cloud-appliance-proxy-repo/appliance.heat.yml

    # Get parameters from parameters.yml into ansible_requirements.yml
    # echo "./prerequisites.sh"
    # ./prerequisites.sh
 
    # Copying ansible_r# equirements.yml generated into appliance dir
    # cp ${LOCAL_PARAMETERS}/nexus_ansible_requirements.yml ${WORKING_DIR}/cloud-appliance-nexus/etc/ansible_requirements.yml
    # cp ${LOCAL_PARAMETERS}/composer_ansible_requirements.yml ${WORKING_DIR}/cloud-appliance-composer/etc/ansible_requirements.yml
    # cp ${LOCAL_PARAMETERS}/proxy_ansible_requirements.yml ${WORKING_DIR}/cloud-appliance-proxy-repo/etc/ansible_requirements.yml

    # Copy owned appliance template into repo generated for run.sh
    # cp ${LOCAL_PARAMETERS}/appliance.heat.nexus4ovh.yml ${WORKING_DIR}/nexus_satis_servers/repo/nexus/appliance.heat.yml
    # cp ${LOCAL_PARAMETERS}/appliance.heat.composer4ovh.yml ${WORKING_DIR}/nexus_satis_servers/repo/composer/appliance.heat.yml
    # cp ${LOCAL_PARAMETERS}/appliance.heat.proxy4ovh.yml ${WORKING_DIR}/nexus_satis_servers/repo/proxy/appliance.heat.yml

 }

push_local_modification2Target() {
        for rep in cloud-appliance-nexus cloud-appliance-composer cloud-appliance-proxy-repo
          do
          echo "cd ${WORKING_DIR}/${rep}"
          cd ${WORKING_DIR}/${rep}
          echo "git commit -a -m \"initial ansible requirements modifications for local gitlab\""
          git commit -a -m "initial ansible requirements modifications for local gitlab"
          echo "git push ${MY_REMOTE}"
          git push ${MY_REMOTE}
          done
 }   

make_nexus_satis_local_changes() {

         printf "%-37b %4b" "copy_local_modification" "[  ]"
         copy_local_modification > /dev/null 2>&1
         printf "%-37b %4b" "\rcopy_local_modification" " [OK]\n"

         #printf "%-37b %4b" "push_local_modification2Target" "[  ]"
         #push_local_modification2Target > /dev/null 2>&1
         #printf "%-37b %4b" "\rpush_local_modification2Target" " [OK]\n"
 }

create_stack() {
    #for stack in proxy nexus composer
    #    do
    #    if [ "$(openstack stack show -c stack_name -f value ${stack})" == "${stack}" ]; then
    #        {
    #        echo "openstack stack delete -y ${stack} --wait"
    #        openstack stack delete -y ${stack} --wait
    #        }
    #    fi
    #    done
    cd ${WORKING_DIR}/nexus_satis_servers
    printf "%-37b %4b" "run.sh" "[  ]"
    ./run.sh #>/dev/null 2>&1
    printf "%-37b %4b" "\rrun.sh" " [OK]\n"

    #echo "openstack server add floating ip cloud-appliance-proxy-repo ${FIP}"
    #openstack server add floating ip cloud-appliance-proxy-repo ${FIP}
 }

set_post_local_environment() {
    set_local_etc_hosts
    set_firewall_rules
}

set_local_etc_hosts() {
    PROXY_SERVER="cloud-appliance-proxy-repo"
    PROXY_IP=$(openstack server show ${PROXY_SERVER} -c addresses -f value | sed -e s'/^.*=\(.*\),.*$/\1/')
    read -p "set PROXY_SERVER=${PROXY_IP} in your /etc/hosts ?
    (you may be invited to su as root)"
    sudo sh -c "sed -i -e 's/^.*nexus-appli.appli.dgfip.*$/${PROXY_IP} nexus-appli.appli.dgfip/g;s/^.*satis-cloud.appli.dgfip.*$/${PROXY_IP} satis-cloud.appli.dgfip/g' /etc/hosts"
    sudo sh -c "sed -i -e 's/^.*nexus-pull.appli.dgfip.*$/${PROXY_IP} nexus-pull.appli.dgfip/g' /etc/hosts"
    sudo sh -c "sed -i -e 's/^.*nexus-push.appli.dgfip.*$/${PROXY_IP} nexus-push.appli.dgfip/g' /etc/hosts"
}

set_proxy_firewall_rules() {
    ssh centos@${IP_PROXY} "sudo sh -c \"firewall-cmd --zone=public --permanent --add-rich-rule='rule family=ipv4 source address=${IP_NEXUS} port port=80 protocol=tcp accept'\""
    ssh centos@${IP_PROXY} "sudo sh -c \"firewall-cmd --zone=public --permanent --add-rich-rule='rule family=ipv4 source address=${IP_NEXUS} port port=8081 protocol=tcp accept'\""
    ssh centos@${IP_PROXY} "sudo sh -c \"firewall-cmd --zone=public --permanent --add-rich-rule='rule family=ipv4 source address=${IP_NEXUS} port port=8082 protocol=tcp accept'\""
    ssh centos@${IP_PROXY} "sudo sh -c \"firewall-cmd --zone=public --permanent --add-rich-rule='rule family=ipv4 source address=${IP_NEXUS} port port=8083 protocol=tcp accept'\""
    ssh centos@${IP_PROXY} "sudo sh -c \"firewall-cmd --zone=public --permanent --add-rich-rule='rule family=ipv4 source address=${IP_COMPOSER} port port=9000 protocol=tcp accept'\""
    ssh centos@${IP_PROXY} "sudo sh -c \"firewall-cmd --zone=public --permanent --add-service=http\""
    ssh centos@${IP_PROXY} "sudo sh -c \"firewall-cmd --reload\""
 }

set_nexus_firewall_rules() {
    ssh centos@${IP_NEXUS} "sudo sh -c \"firewall-cmd --zone=public --permanent --add-rich-rule='rule family=ipv4 source address=${IP_PROXY} port port=8081 protocol=tcp accept'\""
    ssh centos@${IP_NEXUS} "sudo sh -c \"firewall-cmd --zone=public --permanent --add-rich-rule='rule family=ipv4 source address=${IP_PROXY} port port=8082 protocol=tcp accept'\""
    ssh centos@${IP_NEXUS} "sudo sh -c \"firewall-cmd --zone=public --permanent --add-rich-rule='rule family=ipv4 source address=${IP_PROXY} port port=8083 protocol=tcp accept'\""
    ssh centos@${IP_NEXUS} "sudo sh -c \"firewall-cmd --reload\""
 }

set_composer_firewall_rules () {
    ssh centos@${IP_COMPOSER} "sudo sh -c \"firewall-cmd --zone=public --permanent --add-rich-rule='rule family=ipv4 source address=${IP_PROXY} port port=9000 protocol=tcp accept'\""
    ssh centos@${IP_COMPOSER} "sudo sh -c \"firewall-cmd --zone=public --permanent --add-rich-rule='rule family=ipv4 source address=${IP_PROXY} port port=80 protocol=tcp accept'\""
    ssh centos@${IP_COMPOSER} "sudo sh -c \"firewall-cmd --reload\""
}

set_firewall_rules() {
    IP_NEXUS=$(openstack server show cloud-appliance-nexus-repo -c addresses -f value | sed -e s'/^.*=\(.*\),.*$/\1/')
    IP_COMPOSER=$(openstack server show cloud-appliance-composer-repo -c addresses -f value | sed -e s'/^.*=\(.*\),.*$/\1/')
    IP_PROXY=$(openstack server show cloud-appliance-proxy-repo -c addresses -f value | sed -e s'/^.*=\(.*\),.*$/\1/')

    set_proxy_firewall_rules
    set_nexus_firewall_rules
    set_composer_firewall_rules
}

get_var_log_messages() {
    # ssh centos@51.91.22.238 "sh -c 'sudo cp /var/log/messages ~centos/; chown centos:centos ~centos/messages'"
    IP_NEXUS=$(openstack server show cloud-appliance-nexus-repo -c addresses -f value | sed -e s'/^.*=\(.*\),.*$/\1/')
    IP_COMPOSER=$(openstack server show cloud-appliance-composer-repo -c addresses -f value | sed -e s'/^.*=\(.*\),.*$/\1/')
    IP_PROXY=$(openstack server show cloud-appliance-proxy-repo -c addresses -f value | sed -e s'/^.*=\(.*\),.*$/\1/')

    select host in proxy nexus composer all
    do
     case ${host} in
     proxy)
     ssh centos@${IP_PROXY} "sh -c 'sudo cp /var/log/messages ~centos/; sudo chown centos:centos ~centos/messages'"
     scp centos@${IP_PROXY}:~centos/messages ${LOG_DIR}/messages.PROXY
     break
     ;;
     nexus)
     ssh centos@${IP_NEXUS} "sh -c 'sudo cp /var/log/messages ~centos/; sudo chown centos:centos ~centos/messages'"
     scp centos@${IP_NEXUS}:~centos/messages ${LOG_DIR}/messages.NEXUS
     break
     ;;
     composer)
     ssh centos@${IP_COMPOSER} "sh -c 'sudo cp /var/log/messages ~centos/; sudo chown centos:centos ~centos/messages'"
     scp centos@${IP_COMPOSER}:~centos/messages ${LOG_DIR}/messages.COMPOSER
     break
     ;;
     all)
     ssh centos@${IP_PROXY} "sh -c 'sudo cp /var/log/messages ~centos/; sudo chown centos:centos ~centos/messages'"
     scp centos@${IP_PROXY}:~centos/messages ${LOG_DIR}/messages.PROXY
     ssh centos@${IP_NEXUS} "sh -c 'sudo cp /var/log/messages ~centos/; sudo chown centos:centos ~centos/messages'"
     scp centos@${IP_NEXUS}:~centos/messages ${LOG_DIR}/messages.NEXUS
     ssh centos@${IP_COMPOSER} "sh -c 'sudo cp /var/log/messages ~centos/; sudo chown centos:centos ~centos/messages'"
     scp centos@${IP_COMPOSER}:~centos/messages ${LOG_DIR}/messages.COMPOSER
     break
     ;;
     esac
    done
}

init
set_environment

select action in import-one-project \
import-all-projects \
nexus-satis-local-changes \
create-stacks \
install-all-nexus_satis \
set_post_local_environment \
get_var_log_messages \
debug
do
	case $action in
		import-one-project)
			select project in $(cat ${REPO_LIST} | cut -d ";" -f1)
			do
                echo "create_project ${project}"
                create_project ${project}
				break
			done
		break
		;;
		import-all-projects)
        import_all_projects
		break
        ;;
        nexus-satis-local-changes)
        make_nexus_satis_local_changes
        break
        ;;
        create-stacks)
        create_stack
        break
        ;;
        install-all-nexus_satis)
        import_all_projects
        make_nexus_satis_local_changes
        create_stack
        break
        ;;
        set_post_local_environment)
        set_post_local_environment
        break
        ;;
        get_var_log_messages)
        get_var_log_messages
        break
        ;;
        debug)
        echo "debug"
        break
        ;;
	esac
	break
done
