#!/bin/bash
# FMJ DGFiP 2020
# créer un fichier terraform.tfvars à partir des fichiers parametrage yml historique de la PIC
# le fusionner avec les fichiers variables.tf et input.tf
# find . -name "*parameters.yml" -exec grep ': ' {} \; | grep -v '#' | grep -v "\""

#echo > terraform.tfvars

from_yml_to_tfvars() {
    find . -name "*parameters.yml" -exec sed -e's/^.*#.*$//g;s/^  //g;/^$/d;s/: / = /g;/^[[:cntrl:]]/d' {} \; >> terraform.tfvars.RUN
}

from_tf_to_tfvars() {
    find . -path '*variables.tf' -o -path '*input.tf' -exec sed -e's/\(variable \"\)\(.*\)\(\"\)/\2 = \"\"/g;/.*{/!d;s/ {//g' {} \; >> terraform.tfvars.RUN
}

search_tfvars_param_in_yml() {
    for param in $(awk -F'=' '{ print $1 }' terraform.tfvars.RUN)
    do 
        echo "search $param in *_parameters.yml";find . -type f -name '*_parameters.yml' -exec grep $param {} \;
    done
}

search_tf_param_in_yml() {
    for param in $(find . -type f -name '*.tf' -exec grep 'variable \"' {} \; | grep -v '#' | awk -F '"' '{print $2}')
    do 
        #echo "search $param in terraform.tfvars";
        find . -type f -name '*_parameters.yml' -exec grep "${param}: " {} \; 
        #| grep -v ${param}
    done
}

search_yml_param_in_tfvars() {
    for param in $(find . -type f -name '*_parameters.yml' -exec grep ': ' {} \; | grep -v '#' | awk -F ':' '{print $1}')
    do 
        #echo "search $param in terraform.tfvars";
        find . -type f -name 'terraform.tfvars' -exec grep $param {} \; | grep -v ${param}
    done
}

search_empty_param_in_tfvars() {
    for param in $(awk -F'=' '{ print $1 }' terraform.tfvars.RUN)
    do 
        find . -type f -name '*_parameters.yml' -exec grep ${param}: {} | grep ': \"\"' \;
    done
}

clean_format() {
    sort -u terraform.tfvars.RUN | sed -e's/---//g;s/parameters://g;/^$/d' > terraform.tfvars.PRE
    sed -i -e'/^[[:cntrl:]]*$/d;s/[[:cntrl:]]*$//g' terraform.tfvars.PRE
}

unify_parameters () {
OLD_IFS=$IFS
IFS="
"
TOTAL=$(wc -l terraform.tfvars.PRE | awk '{print $1}')
for param in $(cat terraform.tfvars.PRE)
do
#echo -n "[${LOCAL_CPT}]"
# Pour tous les parametres definis
export PARAM=${param%%=*}
# Pour chaque parametres remplis
#echo -n "[${PARAM}][VALUE=$(grep ${PARAM} terraform.tfvars.PRE | grep -v "= \"\s*\"")];"
if [[ $(grep ${PARAM} terraform.tfvars.PRE | grep -v "= \"\s*\"") ]]; then
{
    # grep ${PARAM} terraform.tfvars.PRE | grep -v "= \"\s*\""
    # S'il existe une forme vide
    if [[ $(grep ${PARAM} terraform.tfvars.PRE | grep "= \"\"") ]]; then 
    {
        #echo -n "[EMPTY_VALUE_DETECTED][PROOF=$(grep ${PARAM} terraform.tfvars.PRE | grep "= \"\"")];"
        # on la supprime
        sed -i -e"/${PARAM}= \"\"/d" terraform.tfvars.PRE
    }
    fi
}
fi
((LOCAL_CPT++))
#echo 
done
# il ne reste que les paramètres remplis initialement, les paramètres vides par defaut et les parametres vides dupliqués
uniq -c terraform.tfvars.PRE | sed -e's/^ *[0-9] //g' > terraform.tfvars
}

update_value() {

#BACK_NET_ID=$(openstack network list -f value | grep fmj_net | awk '{print $1}')
#FRONT_NET_ID=${BACK_NET_ID}

BACK_NET_ID=$(openstack network list -f value | grep back | awk '{print $1}')
FRONT_NET_ID=$(openstack network list -f value | grep front | awk '{print $1}')

FLAVOR_ID=$(openstack flavor list -f value | grep  CO1.12 | awk '{print $1}')
IMAGE_ID=$(openstack image list -f value | grep  centos7 | awk '{print $1}')
SECURITY_GROUP=$(openstack security group list -f value | grep ' default ' | awk '{print $1}')


DATAS_SIZE_GB=25
TMP_SIZE_GB=25
GITLAB_SIZE_GB=25
JENKINS_SIZE_GB=25
KEYCLOAK_SIZE_GB=25
NEXUS_SIZE_GB=25
SONARQUBE_SIZE_GB=25

#echo "BACK=${BACK_NET_ID};NET=${FRONT_NET_ID};FLAVOR=${FLAVOR_ID};IMAGE=${IMAGE_ID};SEC=${SECURITY_GROUP};PROJ=${OS_PROJECT_ID}"

sed \
-i \
-e"s/back_net_id = \"\"/back_net_id = \"${BACK_NET_ID}\"/" \
-e"s/front_net_id = \"\"/front_net_id = \"${FRONT_NET_ID}\"/" \
-e"s/os_project_id = \"\"/os_project_id = \"${OS_PROJECT_ID}\"/" \
-e"s/os_auth_url = \"\"/os_auth_url = \"https:\/\/nuage01.dgfip.finances.rie.gouv.fr:5000\/v3\"/" \
-e"s/os_password = \"\"/os_password = \"${OS_PASSWORD}\"/" \
-e"s/os_region_name = \"\"/os_region_name = \"RegionOne\"/" \
-e"s/os_username = \"\"/os_username = \"${OS_USERNAME}\"/" \
-e"s/stack_name = \"\"/stack_name = \"pic-terraform\"/" \
-e"s/default_secgroup_id = \".*\"/default_secgroup_id = \"${SECURITY_GROUP}\"/" \
-e"s/datas_size_Gb = \"\"/datas_size_Gb = \"${DATAS_SIZE_GB}\"/" \
-e"s/tmp_size_Gb = \"\"/tmp_size_Gb = \"${TMP_SIZE_GB}\"/" \
-e"s/gitlab_size_Gb = \"\"/gitlab_size_Gb = \"${GITLAB_SIZE_GB}\"/" \
-e"s/jenkins_size_Gb = \"\"/jenkins_size_Gb = \"${JENKINS_SIZE_GB}\"/" \
-e"s/keycloak_size_Gb = \"\"/keycloak_size_Gb = \"${KEYCLOAK_SIZE_GB}\"/" \
-e"s/nexus_size_Gb = \"\"/nexus_size_Gb = \"${NEXUS_SIZE_GB}\"/" \
-e"s/sonarqube_size_Gb = \"\"/sonarqube_size_Gb = \"${SONARQUBE_SIZE_GB}\"/" \
-e"s/flavor_id = \"\"/flavor_id = \"${FLAVOR_ID}\"/" \
-e"s/image_id = \".*\"/image_id = \"${IMAGE_ID}\"/" \
-e"s/image_name = \"\"/image_name = \"centos7\"/" \
terraform.tfvars

#-e"s/with_ssl = \"\"/with_ssl = \"false\"/" \
#-e"s/domain_suffix = \"\"/domain_suffix = \"appli.dgfip\"/" \
#-e"s/install_nexus = \"\"/install_nexus = \"true\"/" \
#-e"s/backup_nbr = \"\"/backup_nbr = \"1\"/" \
#-e"s/version_tag = \"\"/version_tag = \"v0.9.11\"/" \
#-e"s/appliance_admin_password = \"\"/appliance_admin_password = \"changeme\"/" \


}

select action in all from_yml_to_tfvars from_tf_to_tfvars clean_format unify_parameters update_value finalize search_yml_param_in_tfvars search_tf_param_in_yml
do
case ${action} in
all)
from_yml_to_tfvars
from_tf_to_tfvars
clean_format
unify_parameters
update_value
rm terraform.tfvars.RUN terraform.tfvars.PRE
break;;
from_yml_to_tfvars)
from_yml_to_tfvars
break;;
from_provider_yml_to_tfvars)
from_provider_yml_to_tfvars
break;;
from_user_yml_to_tfvars)
from_user_yml_to_tfvars
break;;
from_tf_to_tfvars)
from_tf_to_tfvars
break;;
from_variables_tf_to_tfvars)
from_variables_tf_to_tfvars
break;;
from_input_tf_to_tfvars)
from_input_tf_to_tfvars
break;;
clean_format)
clean_format
break;;
unify_parameters)
unify_parameters
break;;
update_value)
update_value
break;;
finalize)
cp -rp terraform.tfvars.PRE terraform.tfvars
rm terraform.tfvars.RUN terraform.tfvars.PRE
break;;
search_yml_param_in_tfvars)
search_yml_param_in_tfvars
break;;
search_tf_param_in_yml)
search_tf_param_in_yml
break;;
esac
done