#!/bin/bash
## FMJ 2020 - DGFiP

### Todo: make a makefile!
# blob <- repo <- role <- user
#  |       |       ...
#  v       |
# c/d      v   
# c/d     c/d
#

export WORKSPACE=${1}
[ $(echo ${WORKSPACE} | grep jenkins) ] && {
        JENKINS_MODE=1
	    TEST_REPO="${WORKSPACE}/recette_baw/nexus"
	} || { 
		JENKINS_MODE=0
        TEST_REPO="/home/fredmj/Code/bricabrac/recette_baw/nexus"
	}

LOG_DIR="logs"
LOG_FILE="test-$(date +%Y-%m-%d).log"
RES_FILE="test-$(date +%Y-%m-%d).txt"
echo "TEST-RUN@$(date +%H:%M:%S)" >> $LOG_FILE
echo "JENKINS_MODE=${JENKINS_MODE}" >> $LOG_FILE


test_create_blob() {
 echo "[${0}:${FUNCNAME}]" >> $LOG_FILE
 printf "%-37b %4b" "${action}" "[  ]"
 for actions in ${TEST_REPO}/blobstore/test_create_blobstore.sh ${TEST_REPO}/blobstore/test_blobstore.sh
 do
   ${actions} ${WORKSPACE} >> ${LOG_FILE}
   [ $? -eq '0' ] && printf "%-37b %4b" "${actions##*/} (RC:$?)" " [OK]\n" >> $RES_FILE || printf "%-37b %4b" "${actions##*/} (RC:$?)" " [KO]\n" >> $RES_FILE
 done
 if [ $? -eq '0' ]
    then printf "%-37b %4b" "\r${action} (RC:$?)" " [OK]\n"
    else printf "%-37b %4b" "\r${action} (RC:$?)" " [KO]\n"
 fi
}

test_delete_blob() {
 echo "[${0}:${FUNCNAME}]" >> $LOG_FILE
 for actions in ${TEST_REPO}/blobstore/test_delete_blobstore.sh ${TEST_REPO}/blobstore/test_blobstore.sh
 do
   ${actions} ${WORKSPACE} >> ${LOG_FILE}
   [ $? -eq '0' ] && printf "%-37b %4b" "${actions##*/} (RC:$?)" " [OK]\n" >> $RES_FILE || printf "%-37b %4b" "${actions##*/} (RC:$?)" " [KO]\n" >> $RES_FILE
 done
}

test_create_repo() {
 printf "%-37b %4b" "${action}" "[  ]"
 echo "[${0}:${FUNCNAME}]" >> $LOG_FILE
 for actions in ${TEST_REPO}/repo/test_create_repo.sh ${TEST_REPO}/repo/test_repo.sh
 do
 ${actions} ${WORKSPACE} >> ${LOG_FILE}
 [ $? -eq '0' ] && printf "%-37b %4b" "${actions##*/} (RC:$?)" " [OK]\n" >> $RES_FILE || printf "%-37b %4b" "${actions##*/} (RC:$?)" " [KO]\n" >> $RES_FILE
 done
 if [ $? -eq '0' ]
    then printf "%-37b %4b" "\r${action} (RC:$?)" " [OK]\n"
    else printf "%-37b %4b" "\r${action} (RC:$?)" " [KO]\n"
 fi
}

test_delete_repo() {
 echo "[${0}:${FUNCNAME}]" >> $LOG_FILE
  for actions in ${TEST_REPO}/repo/test_delete_repo.sh ${TEST_REPO}/repo/test_repo.sh
  do
  ${actions} ${WORKSPACE} >> ${LOG_FILE}
  [  $? == 0 ] && printf "%-37b %4b" "${actions##*/} (RC:$?)" " [OK]\n" >> $RES_FILE || printf "%-37b %4b" "${actions##*/} (RC:$?)" " [KO]\n" >> $RES_FILE
  done
}

test_create_role() {
 echo "[${0}:${FUNCNAME}]" >> $LOG_FILE
 for actions in ${TEST_REPO}/role/test_create_role.sh ${TEST_REPO}/role/test_role.sh
 do
 ${actions} ${WORKSPACE} >> ${LOG_FILE}
 [ $? -eq '0' ] && printf "%-37b %4b" "${actions##*/} (RC:$?)" " [OK]\n" >> $RES_FILE || printf "%-37b %4b" "${actions##*/} (RC:$?)" " [KO]\n" >> $RES_FILE
 done
}

test_delete_role() {
 echo "[${0}:${FUNCNAME}]" >> $LOG_FILE
  for actions in ${TEST_REPO}/role/test_delete_role.sh ${TEST_REPO}/role/test_role.sh
  do
  ${actions} ${WORKSPACE} >> ${LOG_FILE}
  [ $? -eq "0" ] && printf "%-37b %4b" "${actions##*/} (RC:$?)" " [OK]\n" >> $RES_FILE || printf "%-37b %4b" "${actions##*/} (RC:$?)" " [KO]\n" >> $RES_FILE
  done
}

test_create_user() {
 echo "[${0}:${FUNCNAME}]" >> $LOG_FILE
  for actions in ${TEST_REPO}/user/test_create_user.sh ${TEST_REPO}/user/test_user.sh
  do
  ${actions} ${WORKSPACE} >> ${LOG_FILE}
  [ $? -eq '0' ] && printf "%-37b %4b" "${actions##*/} (RC:$?)" " [OK]\n" >> $RES_FILE || printf "%-37b %4b" "${actions##*/} (RC:$?)" " [KO]\n" >> $RES_FILE
  done
}

test_delete_user() {
 echo "[${0}:${FUNCNAME}]" >> $LOG_FILE
  for actions in ${TEST_REPO}/user/test_delete_user.sh ${TEST_REPO}/user/test_user.sh
  do
  ${actions} ${WORKSPACE} >> ${LOG_FILE}
  [ $? -eq '0' ] && printf "%-37b %4b" "${actions##*/} (RC:$?)" " [OK]\n" >> $RES_FILE || printf "%-37b %4b" "${actions##*/} (RC:$?)" " [KO]\n" >> $RES_FILE
  done
}

testcase_blobstore() {
 echo "[${0}:${FUNCNAME}]" >> $LOG_FILE
  for tests in test_create_blob test_delete_blob
  do
  printf "%-37b %4b" "${tests}" "[  ]"
  ${tests} >> ${LOG_FILE}
  if [ $? -eq '0' ]
     then printf "%-37b %4b" "${tests} (RC:$?)" " [OK]\n" >> $RES_FILE
     else printf "%-37b %4b" "${tests} (RC:$?)" " [KO]\n" >> $RES_FILE
  fi
  done
}

testcase_repo() {
 echo "[${0}:${FUNCNAME}]" >> $LOG_FILE
  for tests in test_create_blob test_create_repo test_delete_repo test_delete_blob
  do
  printf "%-37b %4b" "${tests}" "[  ]"
  ${tests} >> ${LOG_FILE}
  if [ $? -eq '0' ]
     then printf "%-37b %4b" "${tests} (RC:$?)" " [OK]\n" >> $RES_FILE
     else printf "%-37b %4b" "${tests} (RC:$?)" " [KO]\n" >> $RES_FILE
  fi
  done
}

testcase_role() {
 echo "[${0}:${FUNCNAME}]" >> $LOG_FILE
  for tests in test_create_blob test_create_repo test_create_role test_delete_role test_delete_repo test_delete_blob
  do
  printf "%-37b %4b" "${tests}" "[  ]"
  ${tests} >> ${LOG_FILE} 2>>${LOG_FILE}
  if [ $? -eq 0 ]
     then printf "%-37b %4b" "${tests} (RC:$?)" " [OK]\n" >> $RES_FILE
     else printf "%-37b %4b" "${tests} (RC:$?)" " [KO]\n" >> $RES_FILE
  fi
  done
}

testcase_user() {
 echo "[${0}:${FUNCNAME}]" >> $LOG_FILE
  for tests in test_create_blob test_create_repo test_create_role test_create_user test_delete_user test_delete_role test_delete_repo test_delete_blob
  do
  printf "%-37b %4b" "${tests}" "[  ]"
  ${tests} >> ${LOG_FILE}
  if [ $? -eq '0' ]
     then printf "%-37b %4b" "${tests} (RC:$?)" " [OK]\n" >> $RES_FILE
     else printf "%-37b %4b" "${tests} (RC:$?)" " [KO]\n" >> $RES_FILE
  fi
  done
  printf "%-37b %4b" "${tests}" "[OK]\n"
}

set_openrc_NEXUS_COMPOSER() {
    echo "[${0}:${FUNCNAME}]" >> $LOG_FILE
    # NEXUS-COMPOSER lab project

    export OS_PROJECT_DOMAIN_ID=default
    export OS_PROJECT_ID=878a70ec25b34df9ad47e6949dbf654d
    export OS_REGION_NAME=RegionOne
    export OS_USER_DOMAIN_NAME=Default
    export OS_PROJECT_NAME=DGFIP-PIC-Pic_cloud_dgfip
    export OS_IDENTITY_API_VERSION=3
    export OS_PASSWORD=bah8AhnaeK3r
    export OS_AUTH_URL=https://nuage01.dgfip.finances.rie.gouv.fr:5000/v3
    export OS_USERNAME=dmod-marie-joseph.consultant@dgfip.finances.gouv.fr
    export OS_INTERFACE=public

}

get_nexus_ip() {
    echo "[${0}:${FUNCNAME}]" >> $LOG_FILE
    printf "%-37b %4b" "${0}" "[  ]"
    export IP_NEXUS=$(openstack server show cloud-appliance-nexus-repo -c addresses -f value | sed -e s'/^.*=\(.*\),.*$/\1/')
      if [ $? -eq '0' ]
         then printf "%-37b %4b" "${0} (RC:$?)" " [OK]\n" >> $RES_FILE
         else printf "%-37b %4b" "${0} (RC:$?)" " [KO]\n" >> $RES_FILE
      fi
    echo "IP_NEXUS=${IP_NEXUS}" >> $LOG_FILE
}

get_backup_name() {
    echo "[${0}:${FUNCNAME}]" >> $LOG_FILE
    printf "%-37b %4b" "${0}" "[  ]"
    CRONTAB_SAY=$(ssh centos@${IP_NEXUS} "sudo sh -c \"crontab -l\"")
    VOLUME_NAME=$(echo ${CRONTAB_SAY} | grep 'backup.sh' | sed -e's/^\(.*backup\.sh \)\(.*\)\( \/root.*$\)/\2/g')
      if [ $? -eq '0' ]
         then printf "%-37b %4b" "${0} (RC:$?)" " [OK]\n" >> $RES_FILE
         else printf "%-37b %4b" "${0} (RC:$?)" " [KO]\n" >> $RES_FILE
      fi
    echo "CRONTAB_SAY=${CRONTAB_SAY}" >> $LOG_FILE
    echo "VOLUME_NAME=${VOLUME_NAME}" >> $LOG_FILE
}

get_task_script_id() {
    echo "[${0}:${FUNCNAME}]" >> $LOG_FILE
    printf "%-37b %4b" "${0}" "[  ]"
    TASK_ID=$(curl -s -X GET "http://nexus-cloud.appli-preprod.dgfip/service/rest/v1/tasks?type=script" -H "accept: application/json" -u admin:changeme | jq -Mr '.items[].id')
      if [ $? -eq '0' ]
         then printf "%-37b %4b" "${0} (RC:$?)" " [OK]\n" >> $RES_FILE
         else printf "%-37b %4b" "${0} (RC:$?)" " [KO]\n" >> $RES_FILE
      fi
    echo "TASK_ID=${TASK_ID}" >> $LOG_FILE
}

get_task_status() {
    echo "[${0}:${FUNCNAME}]" >> $LOG_FILE
    printf "%-37b %4b" "${0}" "[  ]"
    TASK_STATUS=$(curl -s -X GET "http://nexus-cloud.appli-preprod.dgfip/service/rest/v1/tasks/${TASK_ID}" -H "accept: application/json" -u admin:changeme | jq -Mr '.currentState')
    echo TASK_STATUS=${TASK_STATUS} >> $LOG_FILE
    while  [ $(curl -s -X GET "http://nexus-cloud.appli-preprod.dgfip/service/rest/v1/tasks/${TASK_ID}" -H "accept: application/json" -u admin:changeme | jq -Mr '.currentState') != "WAITING" ]
        do
        TASK_STATUS=$(curl -s -X GET "http://nexus-cloud.appli-preprod.dgfip/service/rest/v1/tasks/${TASK_ID}" -H "accept: application/json" -u admin:changeme | jq -Mr '.currentState')
        echo "${TASK_ID} TASK_STATUS=${TASK_STATUS} - waiting for task "
        sleep 1
        done
        echo "task done" >> $LOG_FILE
      if [ $? -eq '0' ]
         then printf "%-37b %4b" "${0} (RC:$?)" " [OK]\n" >> $RES_FILE
         else printf "%-37b %4b" "${0} (RC:$?)" " [KO]\n" >> $RES_FILE
      fi
    echo TASK_STATUS=${TASK_STATUS} >> $LOG_FILE
}

run_task_id() {
    echo "[${0}:${FUNCNAME}]" >> $LOG_FILE
    printf "%-37b %4b" "${0}" "[  ]"
    RESULT_TASK=$(curl -s -I -X POST "http://nexus-cloud.appli-preprod.dgfip/service/rest/v1/tasks/${TASK_ID}/run" -H "accept: application/json" -u admin:changeme | grep HTTP/1.1 | sed -e's/\(^HTTP\/1.1 \)\([[:digit:]]*\)\(.*$\)/\2/')
      if [ $? -eq '0' ]
         then printf "%-37b %4b" "${0} (RC:$?)" " [OK]\n" >> $RES_FILE
         else printf "%-37b %4b" "${0} (RC:$?)" " [KO]\n" >> $RES_FILE
      fi
    echo RESULT_TASK=${RESULT_TASK} >> $LOG_FILE
}

backup_volume_now() {
    # Generate a backup based on the crontab task : crontab -l    
    echo "[${0}:${FUNCNAME}]" >> $LOG_FILE
    printf "%-37b %4b" "${0}" "[  ]"
    ssh centos@${IP_NEXUS} "sudo sh -c '/root/backup_script/backup.sh ${VOLUME_NAME} /root/backup_script TEST-NEXUS-BCKP@$(date +%Y-%m-%dT%H:%M:%S)'"
      if [ $? -eq '0' ]
         then printf "%-37b %4b" "${0} (RC:$?)" " [OK]\n" >> $RES_FILE
         else printf "%-37b %4b" "${0} (RC:$?)" " [KO]\n" >> $RES_FILE
      fi
    echo "sudo sh -c '/root/backup_script/backup.sh ${VOLUME_NAME} /root/backup_script NEXUS@$(date +%Y-%m-%dT%H:%M:%S)'" >> $LOG_FILE
}

get_latest_backup_id() {
    echo "[${0}:${FUNCNAME}]" >> $LOG_FILE
    printf "%-37b %4b" "${0}" "[  ]"
    VOLUME_ID=$(openstack volume backup list -f value | grep NEXUS | head -1 | cut -d' ' -f1)
      if [ $? -eq '0' ]
         then printf "%-37b %4b" "${0} (RC:$?)" " [OK]\n" >> $RES_FILE
         else printf "%-37b %4b" "${0} (RC:$?)" " [KO]\n" >> $RES_FILE
      fi
    echo "VOLUME_ID=${VOLUME_ID}" >> $LOG_FILE
}

delete_repo-n-blob() {
    echo "[${0}:${FUNCNAME}]" >> $LOG_FILE
    for actions in test_delete_repo test_delete_blob
      do
      printf "%-37b %4b" "${actions}" "[  ]"
      ${actions} >> ${LOG_FILE}
      if [ $? -eq '0' ]
         then printf "%-37b %4b" "${actions} (RC:$?)" " [OK]\n" >> $RES_FILE
         else printf "%-37b %4b" "${actions} (RC:$?)" " [KO]\n" >> $RES_FILE
      fi
    done
}

restore_volume_now() {
    # Restore the last backup as indicated in the /tmp/nexus-cloud-appliance-nexus-repo.log
    echo "[${0}:${FUNCNAME}]" >> $LOG_FILE
    printf "%-37b %4b" "${0}" "[  ]"
    openstack volume backup list -f value -c ID -c Status | grep ${VOLUME_ID} | cut -d' ' -f2
    while [[ $(openstack volume backup list -f value -c ID -c Status | grep ${VOLUME_ID} | cut -d' ' -f2) != "available" ]]
        do
        echo "wait for backup VOLUME status like available"
        sleep 2
        done
    ssh centos@${IP_NEXUS} "sudo sh -c 'source ~/.bashrc; /root/scripts/nexus/nexus_restore.sh ${VOLUME_ID}'"
      if [ $? -eq '0' ]
         then printf "%-37b %4b" "${0} (RC:$?)" " [OK]\n" >> $RES_FILE && printf "%-37b %4b" "\r${0} (RC:$?)" " [OK]\n"
         else printf "%-37b %4b" "${0} (RC:$?)" " [KO]\n" >> $RES_FILE && printf "%-37b %4b" "\r${0} (RC:$?)" " [OK]\n"
      fi
    echo "ssh centos@${IP_NEXUS} \"sudo sh -c 'source ~/.bashrc; /root/scripts/nexus/nexus_restore.sh ${VOLUME_ID}'\"" >> $LOG_FILE
}

backup() {
    echo "[${0}:${FUNCNAME}]" >> $LOG_FILE
    for actions in set_openrc_NEXUS_COMPOSER get_nexus_ip get_backup_name backup_volume_now  
      do
      printf "%-37b %4b" "${actions}" "[  ]"
      ${actions} >> ${LOG_FILE}
      if [ $? -eq '0' ]
         then printf "%-37b %4b" "${actions} (RC:$?)" " [OK]\n" >> $RES_FILE && printf "%-37b %4b" "\r${0} (RC:$?)" " [OK]\n"
         else printf "%-37b %4b" "${actions} (RC:$?)" " [KO]\n" >> $RES_FILE && printf "%-37b %4b" "\r${0} (RC:$?)" " [OK]\n"
      fi
    done
}

restore(){
    echo "[${0}:${FUNCNAME}]" >> $LOG_FILE
    for actions in set_openrc_NEXUS_COMPOSER get_nexus_ip get_latest_backup_id restore_volume_now
      do
      printf "%-37b %4b" "${actions}" "[  ]"
      ${actions} >> ${LOG_FILE}
      if [ $? -eq '0' ]
         then printf "%-37b %4b" "${actions} (RC:$?)" " [OK]\n" >> $RES_FILE && printf "%-37b %4b" "\r${0} (RC:$?)" " [OK]\n"
         else printf "%-37b %4b" "${actions} (RC:$?)" " [KO]\n" >> $RES_FILE && printf "%-37b %4b" "\r${0} (RC:$?)" " [OK]\n"
      fi
    done
}

backup_delete_restore() {
    echo "[${0}:${FUNCNAME}]" >> $LOG_FILE
    for actions in backup test_delete_repo test_delete_blob get_task_script_id run_task_id get_task_status restore
      do
      printf "%-37b %4b" "${actions}" "[  ]"
      ${actions} >> ${LOG_FILE}
      if [ $? -eq '0' ]
         then printf "%-37b %4b" "${actions} (RC:$?)" " [OK]\n" >> $RES_FILE && printf "%-37b %4b" "\r${0} (RC:$?)" " [OK]\n"
         else printf "%-37b %4b" "${actions} (RC:$?)" " [KO]\n" >> $RES_FILE && printf "%-37b %4b" "\r${0} (RC:$?)" " [OK]\n"
      fi
    done
}

get_log() {
    # ssh centos@{${INSTANCE_IP}} "sh -c 'sudo cp /var/log/messages ~centos/; chown centos:centos ~centos/messages'"
    INSTANCE_NAME="cloud-appliance-proxy-repo cloud-appliance-nexus-repo cloud-appliance-nexus-repo"
    
    for instance in ${INSTANCE_NAME}
     do
         INSTANCE_IP=$(openstack server show ${INSTANCE_NAME} -c addresses -f value | sed -e s'/^.*=\(.*\),.*$/\1/')
         files=$(basename ${path})
         ssh centos@{${INSTANCE_IP}} "sh -c 'sudo cp /var/log/messages ~centos/; chown centos:centos ~centos/messages'"
         scp centos@${INSTANCE_IP}:~centos/messages ${LOG_DIR}/messages.${INSTANCE_NAME}
     done
}

add_nexus_ip_in_knownhosts() {
  # ssh-keyscan -H 51.91.132.72 >> ~/.ssh/known_hosts
  ssh-keyscan -H ${IP_NEXUS} >> ~/.ssh/known_hosts
}

select choice in tests campagne backup delete_repo-n-blob restore backup_delete_restore debug
do
    case ${choice} in
    tests)
     select tests in test_create_blob test_delete_blob test_create_repo test_delete_repo test_create_role test_delete_role test_create_user test_delete_user
     do
            case $tests in
            test_create_blob)
            test_create_blob
            break
            ;;
            test_delete_blob)
            test_delete_blob
            break
            ;;
            test_create_repo)
            test_create_repo
            break
            ;;
            test_delete_repo)
            test_delete_repo
            break
            ;;
            test_create_role)
            test_create_role
            break
            ;;
            test_delete_role)
            test_delete_role
            break
            ;;
            test_create_user)
            test_create_user
            break
            ;;
            test_delete_user)
            test_delete_user
            break
            ;;
            esac
     done
     break
     ;;
    campagne)
     ## Create
     for action in test_create_blob test_create_repo test_create_role test_create_user
     do
     printf "%-37b %4b" "${action}" "[  ]"
     ${action} >> ${LOG_FILE}
     if [ $? -eq '0' ]
       then printf "%-37b %4b" "\r${action} (RC:$?)" " [OK]\n"
       else printf "%-37b %4b" "\r${action} (RC:$?)" " [KO]\n"
     fi
     done
     ## Delete
     for action in test_delete_user test_delete_role test_delete_repo test_delete_blob
     do
     printf "%-37b %4b" "${action}" "[  ]"
     ${action} >> ${LOG_FILE}
     if [ $? -eq '0' ]
       then printf "%-37b %4b" "\r${action} (RC:$?)" " [OK]\n"
       else printf "%-37b %4b" "\r${action} (RC:$?)" " [KO]\n"
     fi
     done
     break
     ;;
    backup)
     backup
     break
     ;;
    delete_repo-n-blob)
     delete_repo-n-blob
     break
     ;;
    restore)
     restore
     break
     ;;
    backup_delete_restore)
     backup_delete_restore
     break
     ;;
    debug)
     get_log
     break
     ;;
    esac
done