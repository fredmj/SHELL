#!/bin/bash
# FMJ 2020 - DGFiP
# ----

exec 2>&-

    LOG_FILE="test-$(date +%Y-%m-%d).log"
    RES_FILE="test-$(date +%Y-%m-%d).txt"
    echo "TEST-RUN-YUM@$(date +%H:%M:%S)" >> $LOG_FILE

mvn_deploy() {

mvn -q -s local.settings.xml deploy:deploy-file \
    -DgroupId=com.fmjconsulting.dgfip.test \
    -DartifactId=package \
    -Dversion=1.0.0 \
    -DgeneratePom=true \
    -Dpackaging=rpm \
    -DrepositoryId=hosted-yum \
    -Durl=http://nexus-cloud.appli-preprod.dgfip/repository/hosted-yum/ \
    -Dfile=rpmbuild/RPMS/x86_64/hello-2.10-1.x86_64.rpm
}

curl_deploy_rpm() {
    curl -v --user 'admin:changeme' --upload-file rpmbuild/RPMS/x86_64/hello-2.10-1.x86_64.rpm  http://nexus-cloud.appli-preprod.dgfip/repository/hosted-yum/
}

    for action in curl_deploy_rpm 
    do
    printf "%-37b %4b" "${action}" "[  ]"
    ${action} >> ${LOG_FILE}
    if [ $? -eq '0' ]
       then printf "%-37b %4b" "\r${action} (RC:$?)" " [OK]\n" && printf "%-37b %4b" "${action} (RC:$?)" " [OK]\n" >> ${RES_FILE}
       else printf "%-37b %4b" "\r${action} (RC:$?)" " [KO]\n" && printf "%-37b %4b" "${action} (RC:$?)" " [OK]\n" >> ${RES_FILE}
    fi
    done
#mvn_deploy