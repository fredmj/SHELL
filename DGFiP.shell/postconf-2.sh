#!/bin/bash
yum install -y maven
curl -sL https://rpm.nodesource.com/setup_13.x | bash -
yum install -y nodejs
yum install -y php
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php composer-setup.php --install-dir=/usr/bin --filename=composer
php -r "unlink('composer-setup.php');"
yum install -y python-pip
pip install selenium
yum install -y python3
pip3 install pytest-selenium
yum install -y docker
systemctl start docker
yum install -y xulrunner
yum install -y chromium-headless
yum install -y google-chrome
yum install -y autoconf
yum install -y autogen
yum install -y automake
yum install -y xorg-x11-util-macros
yum install -y flex
yum install -y bison
yum install -y openssl-devel 
yum install -y bc
yum install -y elfutils-libelf-devel