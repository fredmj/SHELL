# Create a project in gitlab

##### Describe some of needed steps to create a project for a specific user in gitlab.com[^1]


This steps consist mainly in getting the `user_id` from one `user_name` with the appropriate **curl GET :user_id** command. And with this `id`, to use another **curl POST :projects** command to create a project for the user identified by this **id**. Note the GET and POST curl commands embedded in the alias :
```bash
GET.gitlab() {
curl -s --header "PRIVATE_TOKEN=blabla" https://my.gitlab.com/api/v4
}

POST.gitlab() {
curl -s -X POST --header "PRIVATE_TOKEN=blalba" https://my.gitlab.com/api/v4
}
```
 Note that you can

1. Get the `user_id` with you `user_name`: /users?username=`user_name`
  ```bash 
  $ user_id=$(./GET.gitlab /users?username=fredmj | grep "\"id\":" | sed -e's/[^[:digit:]]*//g')
  ```

2. Create the project `my_project` for the user `user_name`
   ```bash
   $DESCRIPTION="This is a short description on one paragraph without any formating tag. For more specifics informations about the project, please use the Readme.md file with markdown formating";./POST.gitlab ${id}/projects?name=`my_project`\&description=$(echo $DESCRIPTION | sed -e\'s/ /\%20/g\')
   ```
   
3. Note you can use the JSON formating for your input with the `-H "Content-Type: applications/json"` and the `--data {"key": "value"}` way or even with the `-d @<file.json>`

4. Note also the **json.tool** python module useful to check _humanly_ the json output format : 

5. ```bash
   curl -X POST bla bla | python -m json.tool
   ```

   [^1]: You need to be gitlab admin to perform this operation. So this **will not** work on you [gitlab.com](https://gitlab.com) even with your owned project. Instead just use the form 

   ```bash
   curl -X POST -H "Private-Token: blabla" https://gitlab.com/api/v4/projects?name=<PROJECT_NAME>\&description=THis%20my%20usual%20Description
   ```

   
