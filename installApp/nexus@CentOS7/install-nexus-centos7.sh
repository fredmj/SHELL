#!/bin/bash
## FMJ 2020
#

echo "Install NEXUS SONATYPE (C) - (TM) on CENTOS7"

useradd nexus

yum install --assumeyes --quiet java-1.8.0-openjdk.x86_64 wget
wget https://download.sonatype.com/nexus/3/latest-unix.tar.gz
wget https://download.sonatype.com/nexus/3/latest-unix.tar.gz.asc

tar --directory=/opt -xz -f latest-unix.tar.gz

mv /opt/nexus* /opt/nexus
chown -R nexus:nexus /opt/nexus /opt/sonatype-work

cat << EOF > /etc/systemd/system/nexus.service
[Unit]
Description=nexus service
After=network.target
  
[Service]
Type=forking
LimitNOFILE=65536
ExecStart=/opt/nexus/bin/nexus start
ExecStop=/opt/nexus/bin/nexus stop
User=nexus
Restart=on-abort
  
[Install]
WantedBy=multi-user.target
EOF

cat << EOF > /opt/nexus/bin/nexus.vmoptions
-Xms2703m
-Xmx2703m
-XX:MaxDirectMemorySize=2703m
-XX:+UnlockDiagnosticVMOptions
-XX:+LogVMOutput
-XX:LogFile=../sonatype-work/nexus3/log/jvm.log
-XX:-OmitStackTraceInFastThrow
-Djava.net.preferIPv4Stack=true
-Dkaraf.home=.
-Dkaraf.base=.
-Dkaraf.etc=etc/karaf
-Djava.util.logging.config.file=etc/karaf/java.util.logging.properties
-Dkaraf.data=../sonatype-work/nexus3
-Dkaraf.log=../sonatype-work/nexus3/log
-Djava.io.tmpdir=../sonatype-work/nexus3/tmp
-Dkaraf.startLocalConsole=false
EOF

systemctl daemon-reload
systemctl enable nexus
systemctl start nexus
