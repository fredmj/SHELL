#!/bin/bash
## FMJ 2020
#

echo "Uninstall NEXUS SONATYPE (C) - (TM) on CENTOS7"

userdel --remove nexus

yum remove --assumeyes --quiet --color=always java-1.8.0-openjdk.x86_64 wget
rm -rf latest-unix.tar.gz
rm -rf latest-unix.tar.gz.asc

rm -rf /opt/nexus*
rm -rf /opt/sonatype*

systemctl stop nexus
systemctl disable nexus

rm /etc/systemd/system/nexus.service

rm /opt/nexus/bin/nexus.vmoptions
