#! /bin/ksh
init () {
	encadre=/home/fredmj/devel/shell/encadre/encadre_2906.sh
FOND="-----------ADMIN-----------






--------------------------"
HAUT=$(echo -e "\033[A")
BAS=$(echo -e "\033[B")
GAUCHE=$(echo -e "\033[D")
DROITE=$(echo -e "\033[C")
set -A TAB_MENU_NVX1
set -A TAB_MENU_NVX2
set -A TAB_MENU_NVX3
set -A TAB_MENU_NVX4
demon=/home/fredmj/devel/shell/demon_v0.3/demon_test.ksh
CURS=1
marge=2
LISTENER_LOCK=0
CONNEXION_LOCK=0
config_file=/home/fredmj/devel/shell/tmp/config_ihmadm.cfg
}
check_config_file () {
	cpt=1
	max__start_level_numb=$(cat $1 | egrep "^<LEVEL_"[0-9]+[0-9]*">$" | wc -l)
	max__end_level_numb=$(cat $1 | egrep "^<\LEVEL_"[0-9]+[0-9]*">$" | wc -l)
#print "max_start=$max__start_level_numb"
#print "max_end=$max__end_level_numb"
while [ $cpt -le $max__start_level_numb ]
	do
	grep "<LEVEL_$cpt>" $config_file >/dev/null && grep "<\LEVEL_$cpt>" $config_file >/dev/null || ERROR_1=1
	cpt=$(($cpt+1))
	done
	cpt=1
##	print "nombre de balises qui definissent le debut/fin d'un niveau de menu"
while [ $cpt -le $max__start_level_numb ]
	do
#		print "niveau_$cpt; <debut>=$(grep "<LEVEL_$cpt>" $config_file | wc -l) <fin>=$(grep "<\LEVEL_$cpt>" $config_file | wc -l)"
		[ $(grep "<LEVEL_$cpt>" $config_file | wc -l) -gt 1 -o $(grep "<LEVEL_$cpt>" $config_file | wc -l ) -eq 0 ] && {
			print "Trop de balises pour le niveau $cpt!"
			ERROR_2=1
		}
	cpt=$(($cpt+1))
	done
[ -z $ERROR_2 ] && print "Concordance balise ouverte/fermee\t\t\t[OK]"
[ -z $ERROR_1 ] && print "Distinction des niveaux de menu\t\t\t\t[OK]"
}
fill_tab_menu () {
bal=1
rec=0
cpt=0
for lines in $(cat $1 )
do
	if [ "$lines" = "<LEVEL_$bal>" ] ; then
		printf "Enreigistrement : niveaux $bal..."
		rec=1
	elif [ "$lines" = "<\LEVEL_$bal>" ] ;then
		printf ".....fin\n"
		rec=0
		cpt=0
		bal=$((bal+1))
	fi
		if [ $rec -eq 1 -a "$lines" != "<\LEVEL_$bal" ] ;then
			TAB_MENU_NVX[$bal$cpt]=$(print $lines | cut -d":" -f1)
			ACTION_TAB_MENU_NVX[$bal$cpt]=$(print $lines | cut -d":" -f2)
			cpt=$((cpt+1))
		fi
done
}
show_menu () {
cpt=0
for files in $(seq 1 50)
do
	[ ! -z ${TAB_MENU_NVX[$cpt]} ] && print "$cpt=${TAB_MENU_NVX[$cpt]}"
	[ ! -z ${ACTION_TAB_MENU_NVX[$cpt]} ] && print "$cpt=${ACTION_TAB_MENU_NVX[$cpt]}"
	cpt=$((cpt+1))
done
}
## Pour la demo
set_menu_1 () {
TAB_MENU_NVX1[0]="1 - LISTENER ON  " 	; ACTION_TAB_MENU_NVX1[0]="listen_on"
TAB_MENU_NVX1[1]="2 - LISTENER OFF " 	; ACTION_TAB_MENU_NVX1[1]="listen_off"
TAB_MENU_NVX1[2]="3 - CONNEXION ON " 	; ACTION_TAB_MENU_NVX1[2]="connexion_on"
TAB_MENU_NVX1[3]="4 - CONNEXION OFF"	; ACTION_TAB_MENU_NVX1[3]="connexion_off"
TAB_MENU_NVX1[4]="4 - MAIN"		; ACTION_TAB_MENU_NVX1[4]=""
TAB_MENU_NVX1[5]="5 - QUIT"		; ACTION_TAB_MENU_NVX1[5]="quit"
}
menu2()
{
	set_menu_2
	menu2_fond
	routine2
}
set_menu_2() {
TAB_MENU_NVX2[0]="1 - Creer une BaseDB"		; ACTION_TAB_MENU_NVX2[0]=""
TAB_MENU_NVX2[1]="2 - Detuire une BaseDB"	; ACTION_TAB_MENU_NVX2[1]=""
TAB_MENU_NVX2[2]="3 - Creer des User" 		; ACTION_TAB_MENU_NVX2[2]=""
TAB_MENU_NVX2[3]="4 - Detruire des User"	; ACTION_TAB_MENU_NVX2[3]=""
TAB_MENU_NVX2[4]="5 - iBACK"			; ACTION_TAB_MENU_NVX2[4]=""
}

mode_icanon () {
tput clear
tput civis
stty -icanon time 1 min 0 -echo 
}
menu1_fond () {
tput cup 0 0
$encadre -g empty_inv -t "$FOND"
cpt=0
tput rmso
while [ $cpt -le $((${#TAB_MENU_NVX1[*]}-1)) ]
	do
		tput cup $((cpt+$marge)) 6 ; printf "${TAB_MENU_NVX1[$cpt]}"
		cpt=$((cpt+1))
	done	
	}
menu2_fond () {
tput cup 0 0
$encadre -g empty_inv -t "$FOND"
cpt=0
tput rmso
while [ $cpt -le $((${#TAB_MENU_NVX2[*]}-1)) ]
	do
		tput cup $((cpt+$marge)) 6 ; printf "${TAB_MENU_NVX2[$cpt]}"
		cpt=$((cpt+1))
	done	
	}
my_encadre () {
	tput cup 11 0
	$encadre -g fill_inv -t"-------------LOG------------
$1"
}
listen_on () {
	if [ $LISTENER_LOCK -eq "0" ] ;then
	OUTPUT=$($demon | cut -d";" -f3) && export LISTENER_LOCK=1
	export PID_DEMON=$(print $OUTPUT | cut -d"=" -f2)
	my_encadre "Listener ON"
else
	my_encadre "Deja ouvert sur $PID_DEMON"
fi
}
listen_off () {
	if [ $LISTENER_LOCK -eq "1" -a $CONNEXION_LOCK -eq "0" ] ;then
		kill -QUIT $PID_DEMON && export LISTENER_LOCK=0
		my_encadre "Listener OFF"
	elif [ $LISTENER_LOCK -eq "1" -a $CONNEXION_LOCK -eq "1" ] ;then
		my_encadre "Connexion toujours ouvertes"
	elif [ $LISTENER_LOCK -eq "0" -a $CONNEXION_LOCK -eq "0" ] ;then
		my_encadre "Aucun Listener ACTIF"
	else
		my_enacdre "Erreur - Disfonctionnement"
	fi
}
connexion_on () {
	if [ $CONNEXION_LOCK -eq "0" -a $LISTENER_LOCK -eq "1" ] ;then
		kill -USR1 $PID_DEMON && export CONNEXION_LOCK=1
		my_encadre "Les Connexions sont ouvertes"
	elif [ $CONNEXION_LOCK -eq "0" -a $LISTENER_LOCK -eq "0" ] ;then
		my_encadre "Pas de Listener : System INACTIF"
	elif [ $CONNEXION_LOCK -eq "1" -a $LISTENER_LOCK -eq "1" ] ;then
		my_encadre "Connexion deja ouverte"
	else
		my_encadre "ERREUR - DISFONCTIONNEMENT"
	fi
}
connexion_off () {
	if [ $CONNEXION_LOCK -eq "1" -a $LISTENER_LOCK -eq "1" ] ;then
		kill -USR2 $PID_DEMON && export CONNEXION_LOCK=0
		my_encadre "Connection OFF"
	elif [ $CONNEXION_LOCK -eq "0" -a $LISTENER_LOCK -eq "1" ] ;then
		my_encadre "Connexion deja fermee"
	elif [ $CONNEXION_LOCK -eq "1" -a $LISTENER_LOCK -eq "0" ] ;then
		my_encadre "DISFONCTIONNEMENT - CONNEXION ZOMBIE"
	elif [ $CONNEXION_LOCK -eq "0" -a $LISTENER_LOCK -eq "0" ] ;then
		my_encadre "Systeme Innactif"
	else
		my_encadre "?erreur?"
	fi
}
quit () {
	if [ $CONNEXION_LOCK -eq "0" -a $LISTENER_LOCK -eq "1" ] ;then 
		kill -QUIT $PID_DEMON 
		stty $sauvegarde_stty
		exit 0
	elif [ $CONNEXION_LOCK -eq "0" -a $LISTENER_LOCK -eq "0" ] ;then 
		stty $sauvegarde_stty
		exit 0
	else
		my_encadre "Connexion toujours ouverte"
	fi
}
routine1 () {
while [ true ]
do
	read
	case $REPLY in
	$HAUT)
	if [ $CURS -gt 0 -a $CURS -le ${#TAB_MENU_NVX1[*]} ] ;then
	tput cup $((CURS+$marge)) 6 ; printf "${TAB_MENU_NVX1[$CURS]}"
	CURS=$((CURS-1))
	tput smso;tput cup $((CURS+$marge)) 6 ; printf "${TAB_MENU_NVX1[$CURS]}";tput rmso
	fi
	;;
	$BAS)
	if [ $CURS -ge 0 -a $CURS -lt $((${#TAB_MENU_NVX1[*]}-1)) ] ;then
	tput cup $((CURS+$marge)) 6 ; printf "${TAB_MENU_NVX1[$CURS]}"
	CURS=$((CURS+1))
	tput smso;tput cup $((CURS+$marge)) 6 ; printf "${TAB_MENU_NVX1[$CURS]}";tput rmso
	fi
	;;
	g)
	if [ -z ${ACTION_TAB_MENU_NVX1[$CURS]} ] ;then
		my_encadre "Pas d'action correspondant � ce choix"
	else
		${ACTION_TAB_MENU_NVX1[$CURS]}
	fi
	;;
	q)
	[ $CONNEXION_LOCK -eq "1" ] && kill -USR2 $PID_DEMON
	[ $LISTENER_LOCK -eq "1" ] && kill -QUIT $PID_DEMON 
	break
	;;
esac
done
}
routine2 () {
while [ true ]
do
	read
	case $REPLY in
	$HAUT)
	if [ $CURS -gt 0 -a $CURS -le ${#TAB_MENU_NVX2[*]} ] ;then
	tput cup $((CURS+$marge)) 6 ; printf "${TAB_MENU_NVX2[$CURS]}"
	CURS=$((CURS-1))
	tput smso;tput cup $((CURS+$marge)) 6 ; printf "${TAB_MENU_NVX2[$CURS]}";tput rmso
	fi
	;;
	$BAS)
	if [ $CURS -ge 0 -a $CURS -lt $((${#TAB_MENU_NVX2[*]}-1)) ] ;then
	tput cup $((CURS+$marge)) 6 ; printf "${TAB_MENU_NVX2[$CURS]}"
	CURS=$((CURS+1))
	tput smso;tput cup $((CURS+$marge)) 6 ; printf "${TAB_MENU_NVX2[$CURS]}";tput rmso
	fi
	;;
	g)
	if [ -z ${ACTION_TAB_MENU_NVX2[$CURS]} ] ;then
		my_encadre "Pas d'action correspondant � ce choix"
	else
		${ACTION_TAB_MENU_NVX2[$CURS]}
	fi
	;;
	q)
	[ $CONNEXION_LOCK -eq "1" ] && kill -USR2 $PID_DEMON
	[ $LISTENER_LOCK -eq "1" ] && kill -QUIT $PID_DEMON 
	break
	;;
esac
done
}

main () {
set_menu_1
menu1_fond
routine1
}
sauvegarde_stty=$(stty -g)
sauvegarde_stty=$(stty -g)
init
#mode_icanon
#main
#set_menu $config_file
#check_config_file $config_file
#fill_tab_menu $config_file
#show_menu
main
stty $sauvegarde_stty
