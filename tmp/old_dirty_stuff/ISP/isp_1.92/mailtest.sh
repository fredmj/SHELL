# parametre du shell
#
MAILER=/usr/lib/sendmail
MIME_FILE=/tmp/testmail.$$

FROM="$(grep $(id -un) /etc/passwd|cut -d: -f5) <$(id -un)@$(uname -n).fr.ec.ps.net>"
#TO=TendjaoM@mail.europcar.com,MariejosF@mail.europcar.com
TO=MariejosF@mail.europcar.com
SUBJECT="Demande d'integration de $2 en $1"

#creation du header MIME du mail

touch $MIME_FILE
#rm $MIME_FILE

echo "From: $FROM ">>$MIME_FILE
echo "To:  $TO ">>$MIME_FILE
echo "Subject: $SUBJECT">>$MIME_FILE
echo 'X-Mailer: INTEG mailer'>>$MIME_FILE
echo 'Content-Type: Multipart/Mixed; boundary="Boundary-=_Separateur"'>>$MIME_FILE
echo 'MIME-Version: 1.0\n'>>$MIME_FILE
echo '--Boundary-=_Separateur\n Content-Type: text/plain\n Content-Transfer-Encoding: 8bit\n'>>$MIME_FILE

# Contenu du message
print "__--**DEMANDE D'INTEGRATION**--__" >>  $MIME_FILE
printf "DEMANDEUR   = $2\n" >> $MIME_FILE
printf "DOMAINE     = $3\n" >> $MIME_FILE
printf "PROJET      = $4\n" >> $MIME_FILE
for PIECE_JOINTE in $(ls ~/bin/integ.txt)
do
   NOM_PIECE_JOINTE=$(basename $PIECE_JOINTE)
   
   # en-tete de la piece jointe 
   echo "--Boundary-=_Separateur\nContent-Type: text/plain;\n name=\"$NOM_PIECE_JOINTE\" ">>$MIME_FILE
   echo "Content-Transfer-Encoding: 7bit\nContent-Disposition: attachment; filename=\"$NOM_PIECE_JOINTE\" ">>$MIME_FILE
   #  la derniere ne contient q'un retour chariot
   echo ''>>$MIME_FILE

   # insertion de la piece jointe
   cat $PIECE_JOINTE>>$MIME_FILE

   #  la derniere ne contient q'un retour chariot
   echo ''>>$MIME_FILE
done

#le separateur final
echo '--Boundary-=_Separateur' >> $MIME_FILE

# envoi du mail
cat  $MIME_FILE |$MAILER -t $TO
#mailx -s $SUBJECT tendjaom@mail.europcar < $MIME_FILE
# destruction du fichier 
rm -f $MIME_FILE