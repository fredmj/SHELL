#! /bin/ksh
#########################################################
#							#
# S'utilise avec proc2.ksh pour illustrer le principe 	#
# du cotraitement de processus et de la redirection	#
# des entrees/sorties.					#
#							#
#							#
#							#
#							#
#							#
#########################################################
print "exec 3< $0"
exec 4> $0
proc1() {
while [ "$DATA" != "quit" ]
do
print ">>>>> proc1 : Listening...."
read DATA
print ">>>>> proc1 : Traitement de $DATA @ $(date)"
done
}
proc1
