#!/bin/ksh
#Script cr�ation de change document
#CC "CSI_ANO" "FICH_ANO"/AFFECTED=("CSI_ANO:PVCS.A;1",) /ATTRIBUTES=(PCKG_ANO="HORS_PACKAGE",SSA_ANO="PVCS",OPEP_ANO="0000",CATEGORIE_ANO="Tests Utilisateurs",GRAVITE_ANO="3_Mineure",PRIORITE_ANO="3_Normale_",CONTEXTE_ANO="Applicatif",QUALIFICATION_ANO="test sur l@'outil @"import_ano@"",BASELINE_ANO="version concernee",,Title="test de l@'outil @"import_ano@"") /HOLD
#Create Change Document - The change document will be CSI_ANO_HELD_6066371
#Operation completed
init () {
MSG_ERR="my_import_ano.ksh - Cr�ation de Change Document.\n
\t\t-a <mode_automatique> : le nom des attributs est plac� sur la premiere ligne\n
\t\t-f <fichier_entree> : fichier de donn�e\n
\t\t-o <fichier_sortie> : script executable"
set -A TAB_ALL
set -A TAB_ATTR
set -A TAB_NAME
set -A TAB_DATA
OLD_IFS=$IFS
}
fill_attribut () {
IFS=";"
local_cpt=0
for lines in $1
do
	TAB_ATTR[$local_cpt]=$lines
	local_cpt=$((local_cpt+1))
done
}
make_TAB () {
local_cpt=0
for attribut in ${TAB_ATTR[*]}
do
	local_var=${TAB_ATTR[$local_cpt]}
	print ${TAB_ATTR[$local_cpt]}
	set -A TAB_NAME_${TAB_ATTR[$local_cpt]}
	print "set -A TAB_NAME_${TAB_ATTR[$local_cpt]}"
	TAB_NAME_${TAB_ATTR[0]}[0]="ok"
	print "var=${TAB_NAME_${TAB_ATTR[0]}[0]}"
	exit 0
	#print "set -A TAB_DATA_${TAB_ATTR[$local_cpt]}"
	local_cpt=$((local_cpt+1))
done
TAB_NAME_${TAB_ATTR[1]}[1]="ok"
print "var=${TAB_NAME_${TAB_ATTR[1]}}"
}
fill_TAB () {
IFS="
"
local_cpt=0
for lines in $(cat $1)
	do
        TAB_ALL[$local_cpt]=$lines
        local_cpt=$((local_cpt+1))
	done
	}
sort_TAB () {
local_cpt=0
for lines in ${TAB_ALL[1]}
do
	print $lines | cut -d";" -f1
done
}
init
fill_attribut $(head -1 $1)
make_TAB
#fill_TAB $1
#sort_TAB
#print "${TAB_ALL[1]}"
#print ${TAB_ATTR[2]}
