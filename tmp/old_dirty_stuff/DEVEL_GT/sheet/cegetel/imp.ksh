#!/bin/ksh
#Script cr�ation de change document
#CC "CSI_ANO" "FICH_ANO"/AFFECTED=("CSI_ANO:PVCS.A;1",) /ATTRIBUTES=(PCKG_ANO="HORS_PACKAGE",SSA_ANO="PVCS",OPEP_ANO="0000",CATEGORIE_ANO="Tests Utilisateurs",GRAVITE_ANO="3_Mineure",PRIORITE_ANO="3_Normale_",CONTEXTE_ANO="Applicatif",QUALIFICATION_ANO="test sur l@'outil @"import_ano@"",BASELINE_ANO="version concernee",,Title="test de l@'outil @"import_ano@"") /HOLD
#Create Change Document - The change document will be CSI_ANO_HELD_6066371
#Operation completed
init () {
[ -f ./script1.tmp ] && rm script1.tmp
[ -f ./script2.tmp ] && rm script2.tmp
[ -f ./script.pvcs ] && rm script.pvcs

MSG_ERR="my_import_ano.ksh - Cr�ation de Change Document.\n
\t\t-a <mode_automatique> : le nom des attributs est plac� sur la premiere ligne\n
\t\t-f <fichier_entree> : nom du fichier de donn�es\n
\t\t-o <fichier_sortie> : nom du script executable"
OLD_IFS=$IFS
PRODUCT=CSI_ANO
WORKSET=WS_ANOMALIE
TYPE_CHDOC=FICH_ANO
VARIANT=A
PCS=1
}
fill_attribut () {
IFS=";"
local_cpt=0
for lines in $1
do
	TAB_ATTR[$local_cpt]=$lines
	#print "TAB($local_cpt)=${TAB_ATTR[$local_cpt]}"
	local_cpt=$((local_cpt+1))
done
#print "MAX ATTR=${#TAB_ATTR[*]}"
}
fill_each_TAB () {
IFS="
"
line_cpt=0
for lines in $(cat $1)
	do
	#var1=$(print "TAB_NAME_${TAB_ATTR[$(($2-1))]}[$line_cpt]=\"$(print $lines | cut -d";" -f$2)\"")
	#eval $var1
	eval $(printf "TAB_NAME_${TAB_ATTR[$(($2-1))]}[$line_cpt]=\"$(print $lines | cut -d";" -f$2)\"")
	#var2=$(printf "\${";printf "TAB_NAME_${TAB_ATTR[$(($2-1))]}[$line_cpt]";print "}")
	#eval print $var2
	#eval print $(printf "\${";printf "TAB_NAME_${TAB_ATTR[$(($2-1))]}[$line_cpt]";print "}")
	line_cpt=$((line_cpt+1))
	done
	}
fill_ALL_TAB () {
	local_cpt=1
	while [ $local_cpt -le $((${#TAB_ATTR[*]}-1)) ]
do
	fill_each_TAB $1 $local_cpt
	local_cpt=$((local_cpt+1))
done
}
make_one_line_script () {
attr_cpt=0
printf "CC \"$PRODUCT\" \"$TYPE_CHDOC\""
printf "/AFFECTED=(\"$PRODUCT:$WORKSET.$VARIANT;$PCS\",)"
printf "/ATTRIBUTES=("
while [ $attr_cpt -le $((${#TAB_ATTR[*]}-1)) ]
do
printf "${TAB_ATTR[$attr_cpt]}=\"$(eval print "$(printf "\${";printf "TAB_NAME_${TAB_ATTR[$attr_cpt]}[$1]";print "}")")\","
attr_cpt=$((attr_cpt+1))
done
printf ","
printf "Title=\"$(eval print "$(printf "\${";printf "TAB_NAME_${TAB_ATTR[1]}[$1]";print "}")")\")"
printf " /HOLD\n" 
}
show_cel_tab () {
	local_cpt=0
	#for lines in $(cat $1)
	for lines in $1
	do
		while [ $local_cpt -le $((${#TAB_ATTR[*]}-1)) ]
		do
			printf "TAB_NAME_${TAB_ATTR[$local_cpt]}[$lines]="
			eval print $(printf "\${";printf "TAB_NAME_${TAB_ATTR[$local_cpt]}[$lines]";print "}")
			local_cpt=$((local_cpt+1))
		done
	done
}
make_script () {
	printf "creer la ligne :"
	line_cpt=1
	for lines in $(cat $1)
	do
		printf "$line_cpt."
		make_one_line_script $line_cpt >> script1.tmp 2>/dev/null
		line_cpt=$((line_cpt+1))
	done
	print "done"
}
formating () {
$(sed -e 's///g' script1.tmp > script2.tmp)
$(sed -e 's/[[:alpha:]]*_*[[:alpha:]]*_*[[:alpha:]]*="",//g' script2.tmp > script.pvcs)
}
init && print "Initialisation \t\t\t[OK]" || print "Initialisation [KO]"
fill_attribut $(head -1 $1) && print "Reconnaissance des attributs \t[OK]" || print "Reconnaissance [KO]"
fill_ALL_TAB $1 && print "Setup des tableaux \t\t[OK]" || print "Setup des tableaux [KO]"
#show_cel_tab 1
make_script $1 && print "Construction du script \t\t[OK]" || print "Construction du script [KO]"
formating && print "Formatage du script \t\t[OK]" || print "Formatage du script [KO]"
