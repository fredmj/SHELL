#! /bin/ksh
#########################################################
#							#
# Tant que l'on a pas redirige l'entree/sortie std du	#
# Co-process, on peut la lire/ecrire avec read/print -p	#
# Sinon (avec un exec "n"</>&p ) on utilise l'option -u	#
# de read/print.					#
#							#
#							#
#							#
#########################################################
p2 () {
##Boucle de communication sur un co-traitement
./proc1.ksh |& 
exec 3>&p
exec 4<&p
while [ "$REPLY" != "quit" ]
do
read -u4 MY_DATA
print "proc2 <<<<<$MY_DATA" ; read
print -u3 "$REPLY"
read -u4 MY_DATA ; print "proc2<<<<<$MY_DATA"
done
}
p3 () {
##Boucle de communication sur un co-traitement
./proc1.ksh |& 
while [ "$REPLY" != "quit" ]
do
read -p MY_DATA
print "proc3 <<<<<$MY_DATA" ; read
print -p "$REPLY"
read -p MY_DATA ; print "proc3<<<<<$MY_DATA"
done
}
p2
#wait
