#! /bin/ksh
#################################################################################
#										#
# Daemon Serveur de connexion							#
# S'utilise avec l'ihm_admin et ihm_client.					#
# Principe : 									#
# Gerer deux FIFO : FIFO_DAT (Donnees) et FIFO_IRQ (Requete)			#
#	Pour ecouter les requetes, echanger des donnees d'identification	#
#	minimales afin d'etablir les parametres necessaires aux processus	#
#	(les descripteurs de fichiers attach�es aux co-processus)		#
#	de cotraitement d�die a un client (2 co-processus dedies aux ..		#
#	entree/sortie entre serveur/client une fois la connexion etablie).	#
#										#
# --------------------------DEMANDE DE Co-Process dediee----------------------	#
# --------------------------A la communication Client/Serveur------------------	#
#										#
#	1-Contact								#
#	Serveur 0===0 : FIFO_DAT						#
#		0===0 : FIFO_IRQ <----- Client (Demande de connexion)		#
#										#
#	2-Reception d'ID process						#
#	Serveur 0===0 : FIFO_DAT <----- Client (Envoie de PID)			#
#		0===0 : FIFO_IRQ						#
#										#
#	3-Demande de login							#
#	Serveur 0===0 : FIFO_DAT						#
#		0===0 : FIFO_IRQ <----- Client (Demande de login)		#
#	 									#
#	4-Reception de login (user system)					#
#	Serveur 0===0 : FIFO_DAT <----- Client (Envoie du login)		#
#		0===0 : FIFO_IRQ						#
#										#
#	5-Demande d'authentification						#
#	Serveur 0===0 : FIFO_DAT						#
#		0===0 : FIFO_IRQ <----- Client (Demande d'authentification)	#
#										#
#	6-Reception de passwd (passwd)						#
#	Serveur 0===0 : FIFO_DAT <----- Client (Envoie du passwd)		#
#		0===0 : FIFO_IRQ						#
#										#
#	7-Envoie d'acreditation (passwd/refus)					#
#	Serveur 0===0 : FIFO_DAT -----> Client (Envoie acreditation/Refus)	#
#		0===0 : FIFO_IRQ						#
#										#
# ----------------------COMMINICATION sur Co-Process dediee--------------------	#
#										#
#	Cr�ation de Co-process avec entree/sortie (fd) au numero d'acreditation	#
#	Gestion des ouvertures/fermeture des descripteurs de fichier		#
#										#
# -----------------------------------------------------------------------------	#
#										#
#	Demande d'ouverture des connexion = FIFO ouvert en lecture.		#
#	Demande de fermeture des connexion = FIFO fermee en lecture/ecriture	#
#	Demande d'arret de service = destruction des FIFOs IRQ/DAT		#
#	Demande d'ouverture de service = creation des FIFOs IRQ/DAT		#
#	Demande de FIN de daemon = Arret de com, destruction des FIFOs IRQ/DAT	#
#										#
#										#
#										#
#################################################################################


init () {
	VERSION=0.4
	export MY_PATH=/home/fredmj/shell/demon_v0.4
	MSG_HELP="demon_test.ksh - Daemon manager de connexion client/serveur. version v$VERSION \n
\t\tUsage\n
\t\t-a : Automatic mode - Ouvre le service et passe en mode d'ecoute des connexions.\n
\t\t-n : Normal mode - Creer le demon et laisse le service ferme.\n
\t\t-h : Aide - Affiche ce message d'Aide.\n
\t\t-v : Version - Affiche le numero de version.\n
\t\tSignaux de commande :\n
\t\tUSR1 - Demande ouverture du service d'ecoute des connexions\n
\t\tUSR2 - Demande fermeture du service d'ecoute des connexions\n
\t\tABRT - Demande Arret du Service\n
\t\tHUP  - Demande Ouverture du Service\n
\t\tQUIT - Fin du Daemon
"
export MESSAGE_SYSTEM="DEMON CLIENT/SERVEUR - MANAGEMENT DE CONNEXION CONCOMITENTE EN KORN SHELL"
}
init_auto () {
	init
	[ ! -p $MY_PATH/FIFO_DAT -o ! -p $MY_PATH/FIFO_IRQ ] && {
	print "Pas de fifo dans le repertoire courant : Tentative de creation de FIFO_DAT et FIFO_IRQ"
	if ! mkfifo -m 000 $MY_PATH/FIFO_DAT 
		then
			print "Pas de creation de file FIFO_DAT possible sur $(pwd) - Probleme de droit possible."; exit 0
		elif ! mkfifo -m 000 $MY_PATH/FIFO_IRQ
		then
			print "Pas de creation de file FIFO_IRQ sur $(pwd) - Problème de droit possible"; exit 0
		else 
			print "FIFO_DAT\t\t[OK]"
			print "FIFO_IRQ\t\t[OK]"
	fi
		}
## Balises globales du service
export ERR_DAT_CLOSE=0 ## Erreure sur demande de fermeture du FIFO de donnee
export ERR_IRQ_CLOSE=0 ## Erreure sur demande de fermeture du FIFO de requete
export ERR_DAT_OPEN=0 ## Erreure sur demande d ouverure du FIFO de donnee
export ERR_IRQ_OPEN=0 ## Erreure sur demande d ouverture du FIFO de requete
export ERR_DAT_DESTR=0 ## erreure sur demande de fin de service
export ERR_IRQ_DESTR=0 ## erreure sur demande de fin de service
export ERR_DAT_CREATE=0 ## erreure sur demande de fin de service
export ERR_IRQ_CREATE=0 ## erreure sur demande d'ouverture de service
export LISTENER_DAT=0 ## temoin d'ecoute des demandes de connexion
export LISTENER_IRQ=0 ## temoin d'ecoute des requetes
export CNX_ALIVE=0
}
reception_de_donnee () {
logger -t "CNX" "Reception de DATA actif"
until [ ! -r $MY_PATH/FIFO_DAT ]
do
read DATA < $MY_FIFO/FIFO_DAT
done
logger -t "CNX" "Reception de DATA termine"
}
envoie_de_donnee () {
until [ ! -w $MY_PATH/FIFO_DAT ]
do
print $1 > $MY_FIFO/FIFO_DAT
done
}
reception_de_requete () {
until [ ! -r $MY_PATH/FIFO_IRQ ]
do
read DATA < $MY_FIFO/FIFO_IRQ
done
}
envoie_de_requete () {
until [ ! -w $MY_PATH/FIFO_IRQ ]
do
print $1 > $MY_FIFO/FIFO_IRQ
done
}
listening () {
print "LISTENING_ON" > $MY_PATH/FIFO_DAT
}
sig_handler () {
	cpt=0
	case $1 in
		"USR1")
		logger "USR1 recus : DEMANDE OUVERTURE DES CONNEXIONS."
		if [ -p $MY_PATH/FIFO_DAT ] ;then 
		chmod 222 $MY_PATH/FIFO_DAT && export LISTENER_DAT=1
		else
		logger -t "USR1" "Pas de TUBE $MY_PATH/FIFO_DAT. Ouverture des droits(+w) impossible"
		export LISTENER_DAT=0
		fi
		if
		[ -p $MY_PATH/FIFO_IRQ ] ;then
		chmod 222 $MY_PATH/FIFO_IRQ && export LISTENER_IRQ=1
		else
		logger -t "USR1" "Pas de TUBE $MY_PATH/FIFO_IRQ. Ouverture des droits (+w) impossible"
		export LISTENER_DAT=0
		fi
		## Si tout vas bien, l'ecoute est confirme OK
		[ -w $MY_PATH/FIFO_DAT -a -w $MY_PATH/FIFO_IRQ ] && export CNX_ALIVE=1
		## Si rien ne vas, l ecoute est confirme KO
		if [ ! -w $MY_PATH/FIFO_DAT -a ! -w $MY_PATH/FIFO_IRQ ] ;then
		export CNX_ALIVE=1
		export ERR_DAT_OPEN=1
		export ERR_IRQ_OPEN=1
		fi
		## Si un des deux FIFO pose probleme on marque les indicateurs
		if [ ! -w $MY_PATH/FIFO_DAT -a -w $MY_PATH/FIFO_IRQ ] ;then
		logger -t "USR1" "Probleme de droit sur le FIFO de donnee" ; export ERR_DAT_OPEN=1 && export CNX_ALIVE=0
		fi
		if [ -w $MY_PATH/FIFO_DAT -a ! -w $MY_PATH/FIFO_IRQ ] ;then 
		logger -t "USR2" "Probleme de droit sur le FIFO de requete" ; export ERR_IRQ_OPEN=1 && export CNX_ALIVE=0
		fi
		;;
		"USR2")
		logger "USR2 recus : DEMANDE FERMETURE DES CONNEXIONS."
		if [ -p $MY_PATH/FIFO_DAT ] ;then
		chmod 000 $MY_PATH/FIFO_DAT 
		else
		logger -t "USR2" "Pas de file $MY_PATH/FIFO_DAT. chmod 000 impossible"
		fi
		if [ -p $MY_PATH/FIFO_IRQ ] ;then
		chmod 000 $MY_PATH/FIFO_IRQ
		else
		logger -t "USR2" "Pas de file $MY_PATH/FIFO_IRQ. chmod 000 impossible"
		fi
		## Si tout vas bien fin d ecoute confirmee OK
		[ ! -w $MY_PATH/FIFO_DAT -a ! -w $MY_PATH/FIFO_IRQ ] && export CNX_ALIVE=0
		## Si rien ne vas tout est confirme KO
		if [  -w $MY_PATH/FIFO_DAT -a -w $MY_PATH/FIFO_IRQ ] ;then
		export CNX_ALIVE=1
		export ERR_DAT_OPEN=1
		export ERR_IRQ_OPEN=1
		fi
		## Si un des deux FIFO pose probleme on marque les indicateurs
		if [ ! -w $MY_PATH/FIFO_DAT -a -w $MY_PATH/FIFO_IRQ ] ;then
		logger -t "USR1" "Probleme de droit sur le FIFO de donnee" ; export ERR_IRQ_OPEN=1 && export CNX_ALIVE=0
		fi
		if [ -w $MY_PATH/FIFO_DAT -a ! -w $MY_PATH/FIFO_IRQ ] ;then 
		logger -t "USR2" "Probleme de droit sur le FIFO de requete" ; export ERR_DAT_OPEN=1 && export CNX_ALIVE=0
		fi
		;;
		"INT")
		logger "INT recus" ;;
		"ABRT")
		logger "ABRT recus : DEMANDE ARRET DU SERVICE"
		##Un test sur LISTENER_IRQ et LISTENER_DAT?!
		[ ! -p $MY_PATH/FIFO_DAT -a ! -p $MY_PATH/FIFO_IRQ -a "$CNX_ALIVE" = "0" ] && logger -t "ABRT" "Service deja fermee"
		if [ -p $MY_PATH/FIFO_DAT -a "$CNX_ALIVE" = "0" ] ;then
		    rm -f $MY_PATH/FIFO_DAT && export LISTENER_DAT=0 && logger -t "ABRT" "FIFO ($MY_PATH/FIFO_DAT) de donnee efface"
		elif  [ -p $MY_PATH/FIFO_DAT -a "$CNX_ALIVE" = "0" ] ;then
		    logger -t "ABRT" "Probleme pour effacer le FIFO de donnee : ($MY_PATH/FIFO_DAT)"
		    export ERR_DAT_DESTR = 1
		elif [ -p $MY_PATH/FIFO_DAT -a "$CNX_ALIVE" = "1" ] ;then
		    logger -t "ABRT" "Service d'ecoute des donnees toujours actif - le stopper d'abord"	
		    export ERR_DAT_DESTR = 1
		fi
		if [ -p $MY_PATH/FIFO_IRQ -a "$CNX_ALIVE" = "0" ] ;then
		    rm -f $MY_PATH/FIFO_IRQ && export LISTENER_IRQ=0 && logger -t "ABRT" "FIFO ($MY_PATH/FIFO_IRQ) de requete efface"
		elif [ -p $MY_PATH/FIFO_DAT -a "$CNX_ALIVE" = "0" ] ;then
		    logger -t "ABRT" "Probleme pour effacer le FIFO de requete : ($MY_PATH/FIFO_IRQ)"
		    export ERR_IRQ_DESTR = 1
		elif [ -p $MY_PATH/FIFO_DAT -a "$CNX_ALIVE" = "1" ] ;then
		    logger -t "ABRT" "Service d'ecoute des requetes toujours actif - le stopper d'abord"	
		    export ERR_DAT_DESTR = 1
		fi
		;;
		"HUP")
		logger "HUP recus : DEMANDE D'OUVERTURE DU SERVCIE"
		[ -p $MY_PATH/FIFO_DAT -a -p $MY_PATH/FIFO_IRQ ] && logger -t "HUP" "Service deja ouvert"
		if [ ! -p $MY_PATH/FIFO_IRQ ] ;then
		    mkfifo -m 000 $MY_PATH/FIFO_IRQ  && logger -t "HUP" "FIFO ($MY_PATH/FIFO_IRQ) de requete cre�"
		    else
		    logger -t "HUP" "Probleme pour creer le FIFO de requete : ($MY_PATH/FIFO_IRQ)"
		    export ERR_IRQ_CREATE = 1
		    fi
		if [ ! -p $MY_PATH/FIFO_DAT ] ;then
		    mkfifo -m 000 $MY_PATH/FIFO_DAT  && logger -t "HUP" "FIFO ($MY_PATH/FIFO_DAT) de donnees cree"
		    else 
		    logger -t "HUP" "Probleme pour creer le FIFO de donnees : ($MY_PATH/FIFO_DAT)"
		    export ERR_DAT_CREATE = 1
		    fi
		;;
		"INT")
		logger "INT reçus" ;;
		"ILL")
		logger "ILL reçus" ;;
		"TRAP")
		logger "TRAP reçus" ;;
		"BUS")
		logger "BUS reçus" ;;
		"FPE")
		logger "FPE reçus" ;;
		"SEGV")
		logger "SEGV reçus" ;;
		"PIPE")
		logger "PIPE reçus" ;;
		"TERM")
		logger "TERM reçus" ;;
		"ALRM")
		logger "ALRM reçus" ;;
		"CHLD")
		logger "CHILD reçus" ;;
		"TTIN")
		logger "TTIN reçus" ;;
		"TTOU")
		logger "TTOU reçus" ;;
		"QUIT")
		logger -t "QUIT" "demande de QUIT"
		if [ -p $MY_PATH/FIFO_DAT -a $LISTENER_DAT = 1 ] ;then
			logger -t "QUIT" "Service d'ecoute de connexion toujours actif...Il faut le stopper d'abord"
		elif [ -p $MY_PATH/FIFO_DAT -a $LISTENER_DAT = 0 ] ;then
			rm -f $MY_PATH/FIFO_DAT && logger -t "QUIT" "rm FIFO_DAT [OK]"
		else
			logger -t "QUIT" "Pas de FIFO : $MY_PATH/FIFO_DAT"
		fi
		if [ -p $MY_PATH/FIFO_IRQ -a $LISTENER_IRQ = 1 ] ;then
			logger -t "QUIT" "Service d'ecoute de requete toujours actif...Il faut le stopper d'abord"
		elif [ -p $MY_PATH/FIFO_IRQ -a $LISTENER_IRQ = 0 ] ;then
			rm -f $MY_PATH/FIFO_IRQ && logger -t "QUIT" "rm FIFO_IRQ [OK]"
		else
			logger -t "QUIT" "Pas de FIFO : $MY_PATH/FIFO_IRQ"
		fi
		[ ! -p $MY_PATH/FIFO_DAT -a ! -p $MY_PATH/FIFO_IRQ ] && logger -t "QUIT" "Fin de DEAEMON [OK]" && export MESSAGE_SYSTEM="Deamon arette." && exit 0 || logger -t "QUIT" "Demande de QUIT [KO]"
		;;
	esac
}
trapping () {
	while true
	do
	trap 'sig_handler USR1' USR1
	trap 'sig_handler USR2' USR2
	trap 'sig_handler QUIT' QUIT
	trap 'sig_handler ABRT' ABRT
	trap 'sig_handler HUP' HUP
	trap 'sig_handler INT' INT
	trap 'sig_handler ILL' ILL
	trap 'sig_handler TRAP' TRAP
	trap 'sig_handler BUS' BUS
	trap 'sig_handler FPE' FPE
	trap 'sig_handler SEGV' SEGV 
	trap 'sig_handler PIPE' PIPE
	trap 'sig_handler TERM' TERM
	trap 'sig_handler ALRM' ALRM
	#trap 'sig_handler CHLD' CHLD
	trap 'sig_handler TTIN' TTIN
	trap 'sig_handler TTOU' TTOU
	done
}
##Moteur!
if [ $PPID = "$PREMIER" ] ; then
	## Je suis le Fils
	logger "$(basename $0) demarre par $(id -un) : PPID=$PPID ; PID=$$;"
	trapping 
else
	## Je suis le pere
	getopts anhv arg
	case $arg in
	"a")
	init_auto 
	#trapping 
	export PREMIER=$$
	PROG=$(cat $0)
	cd /
	setsid /bin/ksh -c "$PROG" "$0" "$@" 0<&- 1>&- 2>&- &
	sleep 1
	print $MESSAGE_SYSTEM
	print "PPID=$PPID;PID=$$;demon=$!"
	;;
	"n")
	init 
	#trapping 
	export PREMIER=$$
	PROG=$(cat $0)
	cd /
	setsid /bin/ksh -c "$PROG" "$0" "$@" 0<&- 1>&- 2>&- &
	sleep 1
	print $MESSAGE_SYSTEM
	print "PPID=$PPID;PID=$$;demon=$!"
	;;
	"h")
	init
	print $MESSAGE_SYSTEM
	print $MSG_HELP
	;;
	"v")
	init
	print $MESSAGE_SYSTEM
	print "Version : v$VERSION"
	;;
	?|*)
	init
	print $MESSAGE_SYSTEM
	print "demon_test.ksh -h pour plus de detail"
	esac
fi
