#! /bin/ksh
init () {
	encadre=/home/fredmj/devel/shell/encadre/encadre_2009.ksh
FOND="-----------CLIENT----------





--------------------------"
HAUT=$(echo -e "\033[A")
BAS=$(echo -e "\033[B")
GAUCHE=$(echo -e "\033[D")
DROITE=$(echo -e "\033[C")
set -A TAB_MENU_NVX1
set -A TAB_MENU_NVX2
set -A TAB_MENU_NVX3
set -A TAB_MENU_NVX4
demon=/home/fredmj/devel/shell/demon_v0.3/demon_test.ksh
CURS=1
marge=2
LISTENER_LOCK=0
CONNEXION_LOCK=0
}
set_menu () {
TAB_MENU_NVX1[0]="1 - CONNEXION ON  " 	; ACTION_TAB_MENU_NVX1[0]="demande_cnx_on"
TAB_MENU_NVX1[1]="2 - CONNEXION OFF " 	; ACTION_TAB_MENU_NVX1[1]="demande_cnx_off"
TAB_MENU_NVX1[2]="3 - " 		; ACTION_TAB_MENU_NVX1[2]=""
TAB_MENU_NVX1[3]="4 - "			; ACTION_TAB_MENU_NVX1[3]=""
TAB_MENU_NVX1[4]="5 - QUIT"		; ACTION_TAB_MENU_NVX1[4]="quit"
}
mode_icanon () {
tput clear
tput civis
stty -icanon time 1 min 0 -echo 
}
menu1_fond () {
tput cup 0 0
$encadre -g empty_inv -t "$FOND"
cpt=0
tput rmso
while [ $cpt -le $((${#TAB_MENU_NVX1[*]}-1)) ]
	do
		tput cup $((cpt+$marge)) 6 ; printf "${TAB_MENU_NVX1[$cpt]}"
		cpt=$((cpt+1))
	done	
	}
my_encadre () {
	tput cup 11 0
	$encadre -g fill_inv -t"-------------LOG------------
$1"
}
my_encadre_blink () {
	while [ true ]
	do
		tput cup 11 0
		my_encadre ""
		$encadre -g fill_inv -t"-------------LOG------------
$1"
	done
}
demande_connexion_on () {
	while [ ! -w $MY_PATH/FIFO_IRQ ]
	do
		my_encadre_blink "Demande de connexion"
	done
	CONNEXION_LOCK=1
	printf "CONNEXION_CALL_FROM_$$" > $MY_FIFO/FIFO_IRQ
}
demande_connexion_off () {
	while [ ! -w $MY_PATH/FIFO_IRQ ]
	do
		my_encadre_blink "Demande de deconnexion"
	done
}
quit () {
	if [ $CONNEXION_LOCK -eq "0" -a $LISTENER_LOCK -eq "1" ] ;then 
		kill -USR2 $PID_DEMON 
		stty $sauvegarde_stty
		exit 0
	elif [ $CONNEXION_LOCK -eq "0" -a $LISTENER_LOCK -eq "0" ] ;then 
		stty $sauvegarde_stty
		exit 0
	else
		my_encadre "Connexion toujours ouverte"
	fi
}
routine1 () {
while [ true ]
do
	read
	case $REPLY in
	$HAUT)
	if [ $CURS -gt 0 -a $CURS -le ${#TAB_MENU_NVX1[*]} ] ;then
	tput rmso;tput cup $((CURS+$marge)) 6 ; printf "${TAB_MENU_NVX1[$CURS]}"
	CURS=$((CURS-1))
	tput smso;tput cup $((CURS+$marge)) 6 ; printf "${TAB_MENU_NVX1[$CURS]}";tput rmso
	fi
	;;
	$BAS)
	if [ $CURS -ge 0 -a $CURS -lt $((${#TAB_MENU_NVX1[*]}-1)) ] ;then
	tput rmso;tput cup $((CURS+$marge)) 6 ; printf "${TAB_MENU_NVX1[$CURS]}"
	CURS=$((CURS+1))
	tput smso;tput cup $((CURS+$marge)) 6 ; printf "${TAB_MENU_NVX1[$CURS]}";tput rmso
	fi
	;;
	g)
	if [ -z ${ACTION_TAB_MENU_NVX1[$CURS]} ] ;then
		tput cup 8 4 ; printf "Pas d'action correspondant � ce choix"
	else
		tput cup 8 4 ; printf "                                     "
		${ACTION_TAB_MENU_NVX1[$CURS]}
	fi
	;;
	q)
	[ $CONNEXION_LOCK -eq "1" ] && kill -USR2 $PID_DEMON
	[ $LISTENER_LOCK -eq "1" ] && kill -QUIT $PID_DEMON 
	break
	;;
esac
done
}
sauvegarde_stty=$(stty -g)
init
set_menu
mode_icanon
menu1_fond
routine1
stty $sauvegarde_stty
