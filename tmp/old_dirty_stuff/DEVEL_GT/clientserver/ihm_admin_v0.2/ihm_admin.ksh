#! /bin/ksh
#################################################
#	IHM_ADM : Administarteur d'IHM		#
#						#
#Reste � faire :				#
# -Adapter la taille du fond en			#
# fonction de la taille du plus grand menu	#
#						#
# -Incorporer les fonctions appellees dans un 	#
# include definissable en ligne de commande	#
#						#
# -Verifier l'existence des fonctions appellees	#
#						#
# 						#
#						#
#						#
#################################################
init () {
[ -z $MY_ROOT ] && {
print "Pas de rep d'install pour shell/ definit - Il faut exporter la variable MY_ROOT."
exit 1
}
encadre=$MY_ROOT/encadre/encadre_2009.ksh

FOND="-----------ADMIN-----------






--------------------------"
HAUT=$(echo -e "\033[A")
BAS=$(echo -e "\033[B")
GAUCHE=$(echo -e "\033[D")
DROITE=$(echo -e "\033[C")
set -A TAB_MENU_NVX
set -A TAB_MENU_NVX1
set -A TAB_MENU_NVX2
set -A TAB_MENU_NVX3
set -A TAB_MENU_NVX4
set -A TAB_CHECK
demon=$MY_ROOT/shell/demon_v0.3/demon_test.ksh
CURS=1
marge=2
LISTENER_LOCK=0
CONNEXION_LOCK=0
config_file=$MY_ROOT/tmp/config_ihmadm.cfg
HLP_MSG="ihm_adm.ksh - Administrateur d'ihm pour shell Korn.\n\
-c <config_file> : Utilise le fichier de configuration <config_file> plutot que celui par defaut.\n\
-d : Mode demonstration.\n\
-h : Affichage de ce message d'Aide.\n\
-f : Full Mode - Mode Complet Client/Serveur - Necessite un ihm_client -.\n\
-r : Restrict Mode : Mode uniquement dedie � l'ihm, sans Client/Serveur.\n\
-v : Verbose Mode : Affiche le diagnostique des traitements.\n\
-k : Check l'environnement."
VERSION="0.4"
}
check_config_file () {
	cpt=1
	max__start_level_numb=$(cat $1 | egrep "^<LEVEL_"[0-9]+[0-9]*">$" | wc -l)
	max__end_level_numb=$(cat $1 | egrep "^<\LEVEL_"[0-9]+[0-9]*">$" | wc -l)
TAB_CHECK[0]=$(print "max_start=$max__start_level_numb")
TAB_CHECK[1]=$(print "max_end=$max__end_level_numb")
if [ ! $max__start_level_numb -eq $max__end_level_numb ] ;then
TAB_CHECK[2]=$(print "Il manque une balise ouverte ou une balise ferm�e")
ERROR_0=1
fi
while [ $cpt -le $max__start_level_numb ]
	do
	grep "<LEVEL_$cpt>" $config_file >/dev/null && grep "<\LEVEL_$cpt>" $config_file >/dev/null || ERROR_1=1
	cpt=$(($cpt+1))
	done
	cpt=1
##	print "nombre de balises qui definissent le debut/fin d'un niveau de menu"
while [ $cpt -le $max__start_level_numb ]
	do
TAB_CHECK[3]=$(print "niveau_$cpt; <debut>=$(grep "<LEVEL_$cpt>" $config_file | wc -l) <fin>=$(grep "<\LEVEL_$cpt>" $config_file | wc -l)")
		[ $(grep "<LEVEL_$cpt>" $config_file | wc -l) -gt 1 -o $(grep "<LEVEL_$cpt>" $config_file | wc -l ) -eq 0 ] && {
			print "Trop de balises pour le niveau $cpt!"
			ERROR_2=1
		}
	cpt=$(($cpt+1))
	done
[ -z $ERROR_2 ] && [ -z $ERROR_0 ] && {
	print "Concordance balise ouverte/fermee\t\t\t[OK]" 
	TAB_CHECK[4]=$(print "Concordance balise ouverte/fermee\t\t\t[OK]") 
} && [ -z $ERROR_1 ] && {
	print "Distinction des niveaux de menu\t\t\t\t[OK]" || exit 0
	TAB_CHECK[5]=$(print "Distinction des niveaux de menu\t\t\t\t[OK]")
} || exit 0
}
fill_tab_menu () {
bal=1
rec=0
cpt=0
for lines in $(cat $1 )
do
	if [ "$lines" = "<LEVEL_$bal>" ] ; then
		TAB_CHECK[6]=$(print "Enregistrement : niveaux $bal...")
		rec=1
	elif [ "$lines" = "<\LEVEL_$bal>" ] ;then
		TAB_CHECK[7]=$(print "Enregistrement : niveaux $bal...fin\n")
		rec=0
		cpt=0
		bal=$((bal+1))
	fi
		if [ $rec -eq 1 -a "$lines" != "<\LEVEL_$bal" ] ;then
			TAB_MENU_NVX[$bal$cpt]=$(print $lines | cut -d":" -f1)
			ACTION_TAB_MENU_NVX[$bal$cpt]=$(print $lines | cut -d":" -f2)
			cpt=$((cpt+1))
		fi
done
}
show_menu () {
start=$(($1*10)) 
stop=$((($1*10)+9))
for files in $( seq $start $stop )
do
	[ ! -z ${TAB_MENU_NVX[$files]} ] && print "$files=${TAB_MENU_NVX[$files]} ${ACTION_TAB_MENU_NVX[$files]}"
done
}
show_check () {
	step=$(($max__start_level_numb%5)) ## On n'affiche ici que des colones de 5 paves..on peut le changer....(attention a la taille de l'ecran)
	local_marge=30 ## En principe egale a la largeur dun tableau - intitul� + action
	level=$max__start_level_numb
	while [ $level -gt 0 ]
	do
		tput cup 3 0
		while [ $step -ge 0 -a $level -gt 0 ]
		do
		$encadre -g empty_normal -m $((160-$local_marge)) -t"$(show_menu $level)"
		step=$(($step-1))
		level=$(($level-1))
		done
	local_marge=$(($local_marge+30))
	step=$(($max__start_level_numb%5))
	done
}
menu_fond () {
	TAB_CHECK[8]=$(print " -- Cr�ation du fond de niveaux $1")
	TAB_CHECK[9]=$(print " -- Recherche du Nombre d'option dans le niveaux $1")
	max_option=$((($1*10)+9))
	TAB_CHECK[10]=$(print "les niveaux de $1 � $max_option")
	cpt=$1
	tput cup 0 0
	$encadre -g empty_inv -t "$FOND"
	tput rmso
	local_cpt=0
	while [ $cpt -le $max_option ]
	do
		[ ! -z ${TAB_MENU_NVX[$cpt]} ] && {
		tput cup $((local_cpt+$marge)) 6 ; printf "${TAB_MENU_NVX[$cpt]}"
		cpt=$((cpt+1))
		local_cpt=$((local_cpt+1))
	} || {
	cpt=$((cpt+1))
	}
	done
TAB_CHECK[11]=$(print " -- Nombre d'option dans le niveaux $1 = $cpt")
MAX_OPTION[$1]=$cpt
}
set_menu_1 () {
TAB_MENU_NVX1[0]="1 - LISTENER ON  " 	; ACTION_TAB_MENU_NVX1[0]="listen_on"
TAB_MENU_NVX1[1]="2 - LISTENER OFF " 	; ACTION_TAB_MENU_NVX1[1]="listen_off"
TAB_MENU_NVX1[2]="3 - CONNEXION ON " 	; ACTION_TAB_MENU_NVX1[2]="connexion_on"
TAB_MENU_NVX1[3]="4 - CONNEXION OFF"	; ACTION_TAB_MENU_NVX1[3]="connexion_off"
TAB_MENU_NVX1[4]="4 - MAIN"		; ACTION_TAB_MENU_NVX1[4]="call_menu 1"
TAB_MENU_NVX1[5]="5 - QUIT"		; ACTION_TAB_MENU_NVX1[5]="quit"
}
## Pour la demo
set_menu_1_demo () {
TAB_MENU_NVX1[0]="1 - Variables d'env" 	; ACTION_TAB_MENU_NVX1[0]="my_encadre $(env)"
TAB_MENU_NVX1[1]="2 - liste du rep" 	; ACTION_TAB_MENU_NVX1[1]="my_encadre $(ls)"
TAB_MENU_NVX1[2]="3 - menu2   " 	; ACTION_TAB_MENU_NVX1[2]="menu2_demo"
TAB_MENU_NVX1[3]="4 - Type de Machine "	; ACTION_TAB_MENU_NVX1[3]="my_encadre $(uname -a)"
TAB_MENU_NVX1[4]="4 - ------- "		; ACTION_TAB_MENU_NVX1[4]=""
TAB_MENU_NVX1[5]="5 - QUIT"		; ACTION_TAB_MENU_NVX1[5]="my_quit"
}
menu1_demo() {
	set_menu_1_demo
	menu1_fond
	routine1
}
menu2_demo() {
	set_menu_2_demo
	menu2_fond
	routine2
}
set_menu_2_demo() {
TAB_MENU_NVX2[0]="1 - Creer une BaseDB"		; ACTION_TAB_MENU_NVX2[0]=""
TAB_MENU_NVX2[1]="2 - Detuire une BaseDB"	; ACTION_TAB_MENU_NVX2[1]=""
TAB_MENU_NVX2[2]="3 - Creer des User" 		; ACTION_TAB_MENU_NVX2[2]=""
TAB_MENU_NVX2[3]="4 - Detruire des User"	; ACTION_TAB_MENU_NVX2[3]=""
TAB_MENU_NVX2[4]="5 - iBACK"			; ACTION_TAB_MENU_NVX2[4]="menu1_demo"
}

mode_icanon () {
tput clear
tput civis
stty -icanon time 1 min 0 -echo 
}
menu1_fond () {
tput cup 0 0
$encadre -g empty_inv -t "$FOND"
cpt=0
tput rmso
while [ $cpt -le $((${#TAB_MENU_NVX1[*]}-1)) ]
	do
		tput cup $((cpt+$marge)) 6 ; printf "${TAB_MENU_NVX1[$cpt]}"
		cpt=$((cpt+1))
	done	
	}
menu2_fond () {
tput cup 0 0
$encadre -g empty_inv -t "$FOND"
cpt=0
tput rmso
while [ $cpt -le $((${#TAB_MENU_NVX2[*]}-1)) ]
	do
		tput cup $((cpt+$marge)) 6 ; printf "${TAB_MENU_NVX2[$cpt]}"
		cpt=$((cpt+1))
	done	
	}
my_encadre () {
	tput cup 11 0
	$encadre -g empty_normal -t"-------------LOG------------
$1"
}
listen_on () {
	if [ $LISTENER_LOCK -eq "0" ] ;then
	OUTPUT=$($demon | cut -d";" -f3) && export LISTENER_LOCK=1
	export PID_DEMON=$(print $OUTPUT | cut -d"=" -f2)
	my_encadre "Listener ON"
else
	my_encadre "Deja ouvert sur $PID_DEMON"
fi
}
listen_off () {
	if [ $LISTENER_LOCK -eq "1" -a $CONNEXION_LOCK -eq "0" ] ;then
		kill -QUIT $PID_DEMON && export LISTENER_LOCK=0
		my_encadre "Listener OFF"
	elif [ $LISTENER_LOCK -eq "1" -a $CONNEXION_LOCK -eq "1" ] ;then
		my_encadre "Connexion toujours ouvertes"
	elif [ $LISTENER_LOCK -eq "0" -a $CONNEXION_LOCK -eq "0" ] ;then
		my_encadre "Aucun Listener ACTIF"
	else
		my_enacdre "Erreur - Disfonctionnement"
	fi
}
connexion_on () {
	if [ $CONNEXION_LOCK -eq "0" -a $LISTENER_LOCK -eq "1" ] ;then
		kill -USR1 $PID_DEMON && export CONNEXION_LOCK=1
		my_encadre "Les Connexions sont ouvertes"
	elif [ $CONNEXION_LOCK -eq "0" -a $LISTENER_LOCK -eq "0" ] ;then
		my_encadre "Pas de Listener : System INACTIF"
	elif [ $CONNEXION_LOCK -eq "1" -a $LISTENER_LOCK -eq "1" ] ;then
		my_encadre "Connexion deja ouverte"
	else
		my_encadre "ERREUR - DISFONCTIONNEMENT"
	fi
}
connexion_off () {
	if [ $CONNEXION_LOCK -eq "1" -a $LISTENER_LOCK -eq "1" ] ;then
		kill -USR2 $PID_DEMON && export CONNEXION_LOCK=0
		my_encadre "Connection OFF"
	elif [ $CONNEXION_LOCK -eq "0" -a $LISTENER_LOCK -eq "1" ] ;then
		my_encadre "Connexion deja fermee"
	elif [ $CONNEXION_LOCK -eq "1" -a $LISTENER_LOCK -eq "0" ] ;then
		my_encadre "DISFONCTIONNEMENT - CONNEXION ZOMBIE"
	elif [ $CONNEXION_LOCK -eq "0" -a $LISTENER_LOCK -eq "0" ] ;then
		my_encadre "Systeme Innactif"
	else
		my_encadre "?erreur?"
	fi
}
my_quit () {
	if [ $CONNEXION_LOCK -eq "0" -a $LISTENER_LOCK -eq "1" ] ;then 
		kill -QUIT $PID_DEMON 
		stty $sauvegarde_stty
		exit 0
	elif [ $CONNEXION_LOCK -eq "0" -a $LISTENER_LOCK -eq "0" ] ;then 
		stty $sauvegarde_stty && stty sane && tput reset
		clear
		exit 0
	else
		my_encadre "Connexion toujours ouverte"
	fi
}
routine () {
CURS=$((($1*10)))
while [ true ]
do
	read
	case $REPLY in
	$HAUT)
	if [ $((ligne+$marge)) -gt 0 -a $CURS -le ${MAX_OPTION[$1]} ] ;then
	tput cup $((ligne+$marge)) 6 ; printf "${TAB_MENU_NVX[$CURS]}"
	CURS=$((CURS-1)) && ligne=$((ligne-1))
	tput smso;tput cup $((ligne+$marge)) 6 ; printf "${TAB_MENU_NVX[$CURS]}";tput rmso
	fi
	;;
	$BAS)
	if [ $((ligne+$marge)) -ge 0 -a $CURS -lt $((${MAX_OPTION[$1]}-1)) ] ;then
	tput cup $((ligne+$marge)) 6 ; printf "${TAB_MENU_NVX[$CURS]}"
	CURS=$((CURS+1)) && ligne=$((ligne+1))
	tput smso;tput cup $((ligne+$marge)) 6 ; printf "${TAB_MENU_NVX[$CURS]}";tput rmso
	fi
	;;
	g)
	if [ -z ${ACTION_TAB_MENU_NVX[$CURS]} ] ;then
		my_encadre "Pas d'action correspondant � ce choix"
	else
		${ACTION_TAB_MENU_NVX[$CURS]}
	fi
	;;
	q)
	stty sane && tput cvvis
	break
	;;
esac
done
}
routine1 () {
while [ true ]
do
	read
	case $REPLY in
	$HAUT)
	if [ $CURS -gt 0 -a $CURS -le ${#TAB_MENU_NVX1[*]} ] ;then
	tput cup $((CURS+$marge)) 6 ; printf "${TAB_MENU_NVX1[$CURS]}"
	CURS=$((CURS-1))
	tput smso;tput cup $((CURS+$marge)) 6 ; printf "${TAB_MENU_NVX1[$CURS]}";tput rmso
	fi
	;;
	$BAS)
	if [ $CURS -ge 0 -a $CURS -lt $((${#TAB_MENU_NVX1[*]}-1)) ] ;then
	tput cup $((CURS+$marge)) 6 ; printf "${TAB_MENU_NVX1[$CURS]}"
	CURS=$((CURS+1))
	tput smso;tput cup $((CURS+$marge)) 6 ; printf "${TAB_MENU_NVX1[$CURS]}";tput rmso
	fi
	;;
	g)
	if [ -z "${ACTION_TAB_MENU_NVX1[$CURS]}" ] ;then
		my_encadre "Pas d'action correspondant � ce choix"
	else
		${ACTION_TAB_MENU_NVX1[$CURS]}
	fi
	;;
	q)
	[ $CONNEXION_LOCK -eq "1" ] && kill -USR2 $PID_DEMON
	[ $LISTENER_LOCK -eq "1" ] && kill -QUIT $PID_DEMON 
	break
	;;
esac
done
}
routine2 () {
while [ true ]
do
	read
	case $REPLY in
	$HAUT)
	if [ $CURS -gt 0 -a $CURS -le ${#TAB_MENU_NVX2[*]} ] ;then
	tput cup $((CURS+$marge)) 6 ; printf "${TAB_MENU_NVX2[$CURS]}"
	CURS=$((CURS-1))
	tput smso;tput cup $((CURS+$marge)) 6 ; printf "${TAB_MENU_NVX2[$CURS]}";tput rmso
	fi
	;;
	$BAS)
	if [ $CURS -ge 0 -a $CURS -lt $((${#TAB_MENU_NVX2[*]}-1)) ] ;then
	tput cup $((CURS+$marge)) 6 ; printf "${TAB_MENU_NVX2[$CURS]}"
	CURS=$((CURS+1))
	tput smso;tput cup $((CURS+$marge)) 6 ; printf "${TAB_MENU_NVX2[$CURS]}";tput rmso
	fi
	;;
	g)
	if [ -z ${ACTION_TAB_MENU_NVX2[$CURS]} ] ;then
		my_encadre "Pas d'action correspondant � ce choix"
	else
		${ACTION_TAB_MENU_NVX2[$CURS]}
	fi
	;;
	q)
	[ $CONNEXION_LOCK -eq "1" ] && kill -USR2 $PID_DEMON
	[ $LISTENER_LOCK -eq "1" ] && kill -QUIT $PID_DEMON 
	break
	;;
esac
done
}
main () {
mode_icanon
set_menu_1
menu1_fond
routine1
}
check () {
check_config_file $config_file
fill_tab_menu $config_file
}	
call_menu () {
menu_fond $1
routine $1
}
[ $# -eq 0 ] && { 
print "ihm_adm.ksh v$VERSION : Tapper ihm_adm.ksh -h Pour plus de d�tails" 
exit 0 
} 
init
sauvegarde_stty=$(stty -g)
while  getopts c:dhfrvk my_arg
do
case $my_arg in
	c)
	[ -z $OPTARG ] || [ $# -eq 2 ] && {
		print "cette option ne s'utilise pas seule. Tapper ihm_adm.ksh -h pour plus de d�tail"
		exit 0
	} || {
		export config_file=$OPTARG
	}
	;;
	d)
	mode_icanon
	menu1_demo
	break
	;;
	h)
	$encadre -g empty_normal -t"$HLP_MSG"
	break
	;;
	f)
	check
	main
	break
	;;
	r)
	check
	mode_icanon
	call_menu 1
	break
	;;
	k)
	check
	show_check 
	;;
	v)
	print "ihm_adm.ksh version v$VERSION - 19/09/03 Frederic MARIE-JOSEPH"
	break
	;;
	?)
	print "Argument inconnue. Tapper ihm_adm.ksh -h pour plus de d�tail."
	exit 0
	;;
	*)
	print "Argument inconnue. Tapper ihm_adm.ksh -h pour plus de d�tail."
	;;	
esac
done
