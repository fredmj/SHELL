#! /bin/ksh
#########################################
# Encadre un texte.		#
#			#
#			#
#			#
#			#
#			#
#			#
#########################################
init () {
HELP_MSG="encadre.sh\t-d : Affiche une demonstration des modes d'encadrement.\n
\t\t-g <mode graph>:Encadre en mode GRAPHIQUE le \"texte\".\n
\t\t\t\tavec <mode grap>= fill_inv.\n
\t\t\t\t\t\t= empty_inv.\n
\t\t\t\t\t\t= empty_normal.\n
\t\t\t\t\t\t= empty.\n
\t\t-a <mode ascii>: Encdare en mode ASCII le \"texte\".\n
\t\t\t\tavec <mod_ascii>= coded.\n
\t\t-t \"texte\": Texte a encadrer.\n
\t\t-m \"char\": Place le Texte a encadrer a <char> de la marge.\n
\t\t-h : Affiche ce message d\'aide."
VERSION="0.2"
set -A LONG_TXT
set -A TRNK_TXT
MY_TMP=$HOME/tmp
MARGE=0
}

top_single_ascii_tab () {
# $1 = length
local_cpt=0
tput smacs
printf %"$MARGE"s"l"
while [ $local_cpt -le $(($1+1)) ]
do
printf "q"
local_cpt=$(($local_cpt+1))
done
printf "k\n"
tput rmacs
}

top_single_graph_tab () {
# $1 = length
local_cpt=0
tput smacs
printf %"$MARGE"s"l"
while [ $local_cpt -le $(($1+1)) ]
do
printf "q"
local_cpt=$(($local_cpt+1))
done
printf "k\n"
tput rmacs
}

lower_single_ascii_tab () {
# $1 = length
local_cpt=0
tput smacs
printf %"$MARGE"s"m"
while [ $local_cpt -le $(($1+1)) ]
do
printf "q"
local_cpt=$(($local_cpt+1))
done
printf "j\n"
tput rmacs
}
lower_single_graph_tab () {
# $1 = length
local_cpt=0
tput smacs
printf %"$MARGE"s"m"
while [ $local_cpt -le $(($1+1)) ]
do
printf "q"
local_cpt=$(($local_cpt+1))
done
printf "j\n"
tput rmacs
}

trunk_void_tab () {
# $1 = length
local_cpt=0
tput smacs
printf %"$MARGE"s"x"
while [ $local_cpt -le $1 ]
do
printf "a"
$(($local_cpt++))
done
printf "x\n"
tput rmacs
}

trunk_fill_tab_graph_centre () {
local_cpt=0
tput smacs
printf %"$MARGE"s"x"
	[ $((($1-$2)%2)) -eq 0 ] && {
	#longueur paire
	while [ "$local_cpt" -le "$((($1-$2)/2))" ]
		do
		printf "a"
		local_cpt=$(($local_cpt+1))
		done
	tput rmacs
	tput smso
	printf "$3" 2>/dev/null
	tput rmso
	tput smacs
	local_cpt=0
	while [ "$local_cpt" -le "$((($1-$2)/2))" ]
		do
		printf "a"
		local_cpt=$((local_cpt+1))
		done
printf "x\n"
	tput rmacs
} || {
	# longueur impaire - on centre legerement sur la droite
	while [ "$local_cpt" -le "$((($1-$2)/2))+1" ]
		do
		printf "a"
		local_cpt=$((local_cpt+1))
		done
	tput rmacs
	tput smso
	printf "$3" 2>/dev/null
	tput rmso
	tput smacs
	local_cpt=0
	while [ $local_cpt -le $((($1-$2)/2)) ]
		do
		printf "a"
		local_cpt=$((local_cpt+1))
		done
printf "x\n"
	tput rmacs
	}
}
trunk_fill_tab_graph_centre_normal () {
local_cpt=0
tput smacs
printf %"$MARGE"s"x"
	[ $((($1-$2)%2)) -eq 0 ] && {
	#longueur paire
	while [ "$local_cpt" -le "$((($1-$2)/2))" ]
		do
		printf "a"
		local_cpt=$(($local_cpt+1))
		done
	tput rmacs
	#tput smso
	printf "$3" 2>/dev/null
	#tput rmso
	tput smacs
	local_cpt=0
	while [ "$local_cpt" -le "$((($1-$2)/2))" ]
		do
		printf "a"
		local_cpt=$((local_cpt+1))
		done
printf "x\n"
	tput rmacs
} || {
	# longueur impaire - on centre legerement sur la droite
	while [ "$local_cpt" -le "$((($1-$2)/2))+1" ]
		do
		printf "a"
		local_cpt=$((local_cpt+1))
		done
	tput rmacs
	#tput smso
	printf "$3" 2>/dev/null
	#tput rmso
	tput smacs
	local_cpt=0
	while [ $local_cpt -le $((($1-$2)/2)) ]
		do
		printf "a"
		local_cpt=$((local_cpt+1))
		done
printf "x\n"
	tput rmacs
	}
}
trunk_fill_tab_graph_inv_empty_centre () {
local_cpt=0
tput smacs
printf %"$MARGE"s"x"
	[ $((($1-$2)%2)) -eq 0 ] && {
	#longueur paire
	while [ "$local_cpt" -le "$((($1-$2)/2))" ]
		do
		printf " "
		local_cpt=$(($local_cpt+1))
		done
	tput rmacs
	tput smso
	printf "$3" 2>/dev/null
	tput rmso
	tput smacs
	local_cpt=0
	while [ "$local_cpt" -le "$((($1-$2)/2))" ]
		do
		printf " "
		local_cpt=$((local_cpt+1))
		done
printf "x\n"
	tput rmacs
} || {
	# longueur impaire - on centre legerement sur la droite
	while [ "$local_cpt" -le "$((($1-$2)/2))+1" ]
		do
		printf " "
		local_cpt=$((local_cpt+1))
		done
	tput rmacs
	tput smso
	printf "$3" 2>/dev/null
	tput rmso
	tput smacs
	local_cpt=0
	while [ $local_cpt -le $((($1-$2)/2)) ]
		do
		printf " "
		local_cpt=$((local_cpt+1))
		done
printf "x\n"
	tput rmacs
	}
}
trunk_fill_tab_graph_normal_empty_centre () {
local_cpt=0
tput smacs
printf %"$MARGE"s"x"
	[ $((($1-$2)%2)) -eq 0 ] && {
	#longueur paire
	while [ "$local_cpt" -le "$((($1-$2)/2))" ]
		do
		printf " "
		local_cpt=$(($local_cpt+1))
		done
	tput rmacs
	#tput smso
	printf "$3" 2>/dev/null
	#tput rmso
	tput smacs
	local_cpt=0
	while [ "$local_cpt" -le "$((($1-$2)/2))" ]
		do
		printf " "
		local_cpt=$((local_cpt+1))
		done
printf "x\n"
	tput rmacs
} || {
	# longueur impaire - on centre legerement sur la droite
	while [ "$local_cpt" -le "$((($1-$2)/2))+1" ]
		do
		printf " "
		local_cpt=$((local_cpt+1))
		done
	tput rmacs
	#tput smso
	printf "$3" 2>/dev/null
	#tput rmso
	tput smacs
	local_cpt=0
	while [ $local_cpt -le $((($1-$2)/2)) ]
		do
		printf " "
		local_cpt=$((local_cpt+1))
		done
printf "x\n"
	tput rmacs
	}
}
trunk_fill_tab_ascii_centre () {
local_cpt=0
printf "|"
	[ $((($1-$2)%2)) -eq 0 ] && {
	#longueur paire
	while [ "$local_cpt" -le "$((($1-$2)/2))" ]
		do
		printf " "
		local_cpt=$(($local_cpt+1))
		done
	printf "$3" 2>/dev/null
	local_cpt=0
	while [ "$local_cpt" -le "$((($1-$2)/2))" ]
		do
		printf " "
		local_cpt=$((local_cpt+1))
		done
printf "|\n"
} || {
	# longueur impaire - on centre legerement sur la droite
	while [ "$local_cpt" -le "$((($1-$2)/2))+1" ]
		do
		printf " "
		local_cpt=$((local_cpt+1))
		done
	printf "$3" 2>/dev/null
	local_cpt=0
	while [ $local_cpt -le $((($1-$2)/2)) ]
		do
		printf " "
		local_cpt=$((local_cpt+1))
		done
printf "|\n"
	}
}

draw_single_tab () {
# $1 = length et $2 = height
top_single_tab $1
trunk_cpt=0
while [ $trunk_cpt -le $2 ]
do
trunk_void_tab $1
$(($trunk_cpt++))
done
lower_single_tab $1
}

set_max_length () {
## Comment faire un sort alphabetique sans passer par un fichier...? un echo -e "a\nb\n...." | sort marche en ligne de cmd!
## on pourrait utiliser tr pour transformer les espaces du texte pass� en argument, en '\n', et ensuite le piper vers sort...
## par exe ....func () { tr $1 -subsitute " " en "\n" | echo -e | sort | tail -1} on obtiendrait le plus grand (petit) d'une
## liste pass� en argument.
for long in $(printf "${LONG_TXT[*]}")
do
print "$long\n" >> $MY_TMP/tmp_sort
done
#print "le plus long= $(print $(sort -d -b -u $MY_TMP/tmp_sort ) | cut -f1 -d " ")"
#export max_length=$(print $(sort -d -b -u $MY_TMP/tmp_sort ) | cut -f1 -d " ")
export max_length=$(print $(sort -d -b -u -r -n $MY_TMP/tmp_sort ) | cut -d " " -f1)
#print "max_lenght=$max_length"
rm -f $MY_TMP/tmp_sort
}

print_trunk () {
local_cpt=0
while [ $local_cpt -le $(printf "$1\n" | wc -l) ]
do
print "trunk_fill_tab $max_lenght ${LONG_TXT[$local_cpt]} ${TRNK_TXT[$local_cpt]}"
done
}
status () {
print "var1=$(printf "$1\n" | wc -l)"
print "var2=$(printf "$1" 2>/dev/null | cut -f$local_cpt -d "
")"
print "longeur_max=$max_lenght ${LONG_TXT[$local_cpt]}"
print "texte trunk=${TRNK_TXT[$local_cpt]}"
}

###############
#  ASCII	 #
###############

tab_ascii ()  {
local_cpt=1

while [ "$local_cpt" -le "$(printf "$1\n" | wc -l)" ]
do
var=$(printf "$1" 2>/dev/null | cut -f$local_cpt -d "
")
#print "trunk_$local_cpt= $(printf "$1" 2>/dev/null | cut -f$local_cpt -d "
#") - longueur = ${#var}" # pour verifier...
TRNK_TXT[$local_cpt]=$var
LONG_TXT[$local_cpt]=${#var}
#$(($local_cpt++))
local_cpt=$(($local_cpt+1))
done

set_max_length
my_local_cpt=1
#print "longueur du tronc=$(printf "$1\n" | wc -l)"
top_single_ascii_tab $max_length
while [ "$my_local_cpt" -le "$(printf "$1\n" | wc -l)" ]
do
#print "longueur du tronc actuel=$(printf "$1\n" | wc -l)"
#print "trunk_fill_tab_test arg1=$max_length arg2=${LONG_TXT[$local_cpt]} arg3=${TRNK_TXT[$local_cpt]}"
trunk_fill_tab_ascii_centre "$max_length" ${LONG_TXT[$my_local_cpt]} "${TRNK_TXT[$my_local_cpt]}"
my_local_cpt=$(($my_local_cpt+1))
done
lower_single_ascii_tab $max_length
}

###############
# Graph Fill inv       #
###############

tab_graph_fill_inv ()  {
TERM=vt320
local_cpt=1

while [ "$local_cpt" -le "$(printf "$1\n" | wc -l)" ]
do
var=$(printf "$1" 2>/dev/null | cut -f$local_cpt -d "
")
#print "trunk_$local_cpt= $(printf "$1" 2>/dev/null | cut -f$local_cpt -d "
#") - longueur = ${#var}" # pour verifier...
TRNK_TXT[$local_cpt]=$var
LONG_TXT[$local_cpt]=${#var}
#$(($local_cpt++))
local_cpt=$(($local_cpt+1))
done

set_max_length
my_local_cpt=1
#print "longueur du tronc=$(printf "$1\n" | wc -l)"
top_single_graph_tab $max_length
while [ "$my_local_cpt" -le "$(printf "$1\n" | wc -l)" ]
do
#print "longueur du tronc actuel=$(printf "$1\n" | wc -l)"
#print "trunk_fill_tab_test arg1=$max_length arg2=${LONG_TXT[$local_cpt]} arg3=${TRNK_TXT[$local_cpt]}"
trunk_fill_tab_graph_centre "$max_length" ${LONG_TXT[$my_local_cpt]} "${TRNK_TXT[$my_local_cpt]}"
my_local_cpt=$(($my_local_cpt+1))
done
lower_single_graph_tab $max_length
}
###############
# Graph Fill normal       #
###############

tab_graph_fill_normal ()  {
TERM=vt320
local_cpt=1

while [ "$local_cpt" -le "$(printf "$1\n" | wc -l)" ]
do
var=$(printf "$1" 2>/dev/null | cut -f$local_cpt -d "
")
#print "trunk_$local_cpt= $(printf "$1" 2>/dev/null | cut -f$local_cpt -d "
#") - longueur = ${#var}" # pour verifier...
TRNK_TXT[$local_cpt]=$var
LONG_TXT[$local_cpt]=${#var}
#$(($local_cpt++))
local_cpt=$(($local_cpt+1))
done

set_max_length
my_local_cpt=1
#print "longueur du tronc=$(printf "$1\n" | wc -l)"
top_single_graph_tab $max_length
while [ "$my_local_cpt" -le "$(printf "$1\n" | wc -l)" ]
do
#print "longueur du tronc actuel=$(printf "$1\n" | wc -l)"
#print "trunk_fill_tab_test arg1=$max_length arg2=${LONG_TXT[$local_cpt]} arg3=${TRNK_TXT[$local_cpt]}"
trunk_fill_tab_graph_centre_normal "$max_length" ${LONG_TXT[$my_local_cpt]} "${TRNK_TXT[$my_local_cpt]}"
my_local_cpt=$(($my_local_cpt+1))
done
lower_single_graph_tab $max_length
}
###############
# Graph empty inv#
###############

tab_graph_empty_inv ()  {
TERM=vt320
local_cpt=1

while [ "$local_cpt" -le "$(printf "$1\n" | wc -l)" ]
do
var=$(printf "$1" 2>/dev/null | cut -f$local_cpt -d "
")
#print "trunk_$local_cpt= $(printf "$1" 2>/dev/null | cut -f$local_cpt -d "
#") - longueur = ${#var}" # pour verifier...
TRNK_TXT[$local_cpt]=$var
LONG_TXT[$local_cpt]=${#var}
#$(($local_cpt++))
local_cpt=$(($local_cpt+1))
done

set_max_length
my_local_cpt=1
#print "longueur du tronc=$(printf "$1\n" | wc -l)"
top_single_graph_tab $max_length
while [ "$my_local_cpt" -le "$(printf "$1\n" | wc -l)" ]
do
#print "longueur du tronc actuel=$(printf "$1\n" | wc -l)"
#print "trunk_fill_tab_test arg1=$max_length arg2=${LONG_TXT[$local_cpt]} arg3=${TRNK_TXT[$local_cpt]}"
trunk_fill_tab_graph_inv_empty_centre "$max_length" ${LONG_TXT[$my_local_cpt]} "${TRNK_TXT[$my_local_cpt]}"
my_local_cpt=$(($my_local_cpt+1))
done
lower_single_graph_tab $max_length
}
###############
# Graph empty normal#
###############

tab_graph_empty_normal ()  {
TERM=vt320
local_cpt=1

while [ "$local_cpt" -le "$(printf "$1\n" | wc -l)" ]
do
var=$(printf "$1" 2>/dev/null | cut -f$local_cpt -d "
")
#print "trunk_$local_cpt= $(printf "$1" 2>/dev/null | cut -f$local_cpt -d "
#") - longueur = ${#var}" # pour verifier...
TRNK_TXT[$local_cpt]=$var
LONG_TXT[$local_cpt]=${#var}
#$(($local_cpt++))
local_cpt=$(($local_cpt+1))
done

set_max_length
my_local_cpt=1
#print "longueur du tronc=$(printf "$1\n" | wc -l)"
top_single_graph_tab $max_length
while [ "$my_local_cpt" -le "$(printf "$1\n" | wc -l)" ]
do
#print "longueur du tronc actuel=$(printf "$1\n" | wc -l)"
#print "trunk_fill_tab_test arg1=$max_length arg2=${LONG_TXT[$local_cpt]} arg3=${TRNK_TXT[$local_cpt]}"
trunk_fill_tab_graph_normal_empty_centre "$max_length" ${LONG_TXT[$my_local_cpt]} "${TRNK_TXT[$my_local_cpt]}"
my_local_cpt=$(($my_local_cpt+1))
done
lower_single_graph_tab $max_length
}

###############
#   Demo      #
###############
demo () {
tab_ascii "$*"
tab_graph_fill_inv "$*"
tab_graph_fill_normal "$*"
tab_graph_empty_inv "$*"
tab_graph_empty_normal "$*"
}
############
##   Main    ##
############
trunk_fill_tab_graph_centre () {
local_cpt=0
tput smacs
printf %"$MARGE"s"x"
	[ $((($1-$2)%2)) -eq 0 ] && {
	#longueur paire
	while [ "$local_cpt" -le "$((($1-$2)/2))" ]
		do
		printf "a"
		local_cpt=$(($local_cpt+1))
		done
	tput rmacs
	tput smso
	printf "$3" 2>/dev/null
	tput rmso
	tput smacs
	local_cpt=0
	while [ "$local_cpt" -le "$((($1-$2)/2))" ]
		do
		printf "a"
		local_cpt=$((local_cpt+1))
		done
printf "x\n"
	tput rmacs
} || {
	# longueur impaire - on centre legerement sur la droite
	while [ "$local_cpt" -le "$((($1-$2)/2))+1" ]
		do
		printf "a"
		local_cpt=$((local_cpt+1))
		done
	tput rmacs
	tput smso
	printf "$3" 2>/dev/null
	tput rmso
	tput smacs
	local_cpt=0
	while [ $local_cpt -le $((($1-$2)/2)) ]
		do
		printf "a"
		local_cpt=$((local_cpt+1))
		done
printf "x\n"
	tput rmacs
	}
}
init
# print "nombre argument $# and OPTARG=$OPTARG"
[ $# -eq 0 ] && {
	print $HELP_MSG
	exit 1
}
while getopts hdg:a:t:m: my_arg ; do
case $my_arg in
	h)
	print $HELP_MSG
	exit 0
	;;
	d | "demo")
	demo "Ceci est une demonstration
sur plusieurs lignes..."
	exit 0
	;;
	g)
	[ $OPTARG = "fill_inv" -o $OPTARG = "empty_inv" -o $OPTARG = "empty" -o $OPTARG = "empty_normal" ] && {
	MOD=$OPTARG
	} || {
	print "mauvais type graphique : fill_inv ou empty_inv"
	exit 1
	}
	;;
	a)
	[ $OPTARG = "coded" ] && {
	MOD=ASCII_$OPTARG
	}
	;;
	t)
	TXT=$OPTARG
	;;
	m)
	MARGE=$OPTARG
	;;
	?)
	print "Mauvais argument, essayer encadre.sh -h pour plus d'information"
	exit 1
	;;
esac
done
[ $MOD = "ASCII_coded" ] && tab_ascii "$TXT"
[ $MOD = "fill_inv" ] && tab_graph_fill_inv "$TXT"
[ $MOD = "empty" ] && tab_graph_fill_normal "$TXT"
[ $MOD = "empty_inv" ] && tab_graph_empty_inv "$TXT"
[ $MOD = "empty_normal" ] && tab_graph_empty_normal "$TXT"

