#Some library for bash array manipulations

function Rec_TAB_From_File() {
## Usage Rec_TAB_From_File $1="INPUT_FILE" $2="TAB_NAME" $3="LINE HEADER #"
## SEPARATE_NUMBER is (n-1) COLUMN_NUMBER

INPUT_FILE=$1
TAB_NAME=$2
HEADER_LINE=$3

echo "INPUT_FILE=$1"
echo "TAB_NAME = $2"
echo "HEADER_LINE = $3"
echo "Into the Function - PID=$PPID"

SEPARATE_NUMBER=35

declare -A eval echo $2
#tableau dynamique environnement git for windows (mingw64/bash)
#eval ${2}[0]="ok"
# attribut la valeur "ok" au premier element du tableau nommé par le nom passé à la fonction en seconde variable "${2}"
#eval echo \${$2[0]}
# affichage du premier element du tableau ${2}

local_cpt=0
IFS="
"
for lines in $(head $INPUT_FILE)
do
eval ${2}[$local_cpt]=\"$(echo $lines | sed -e's/"//g')\"
local_cpt=$(($local_cpt+1))
done

echo "Into the Function - echo LREF_TOL[0]=${LREF_TOL[0]}"
return "ok"

#RUNTIME[0]=`eval echo \${$2[0]}`
#echo "Into the Function - RUNTIME[0]=${RUNTIME[0]}"
}

function cprint { 
echo -ne "$(tput setaf $1)${2}$(tput sgr0)" 
}
