#!/bin/bash
########################################
#
#	EC:A8:6B:F4:E5:E3
#	MAC [i|e]NUC1
#

MAC=EC:A8:6B:F4:E5:E3
BORADCAST=192.168.1.255
PortNumber=9

echo -e $(echo $(printf 'f%.0s' {1..12}; printf "$(echo $MAC | sed 's/://g')%.0s" {1..16}) | sed -e 's/../\\x&/g') | nc -w1 -u 192.168.1.255 $PortNumber

