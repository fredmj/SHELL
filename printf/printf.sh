#!/bin/bash

width=10
for data in {1..10}
  do
  printf "[%0*d] %s\r" ${width} ${data} "your message here"
  sleep 1
  done
  printf "[%0*d] %s\n" ${width} ${data} "Done"
