#!/bin/bash
#########################################
# FMJ 2017 vboxlib			#
#					#
# quirky tools for automated set up 	#
# environments with Oracle(c) virtualbox#
#					#
#########################################


ShowVms() {
# Get a look over vms, runnings or not
}

ShowLabs() {
# Show entry like (OS;TypeName) ie (CentOS;CI labs #1) or (RedHat;OpenStack lab full)
# more info with an option like -a, which give all the components details
}

StartVm() {
# Start a vm
}

CloneVm() {
# Clone a vm
}

ShowServer() {
# Show entry like (OS;TypeName) ie (CentOS;apache) or (RedHat;OpenStack)
# more info with an option like -a, which give all the components details
}

MakeServer() {
# Create a server based on a cookbook describing how to setup an application
# ie MakeServerType "CentOS Keystone" by : 
# Specify an OS
# Specify the way to create one : automatic or interactive
# Automatic mean : gimme a type of item -ie proc number, memory amount, applications like apache, jenkins, gitlab, ...
# Interactive mean : lets get some choices with menu to specify the machine and there software.
# Adding applicationis like apache, NodeJS, mariadb

}

ShowGroupe() {
# Groups are serie of servers.
# For example: 'CI Lab #1'={(Ubuntu;git),(CentOS;gitlab),(CentOS;jenkins)}
}

CreateGroup() {
# Make a group of Server already defined
}


