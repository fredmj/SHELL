#!/bin/bash
#############################################
# FMJ 2017 aug                              #
#                                           #
#                                           #
#                                           #
#############################################

for uuid in $(vboxmanage list vms | cut -d '{' -f2)
  do
  ID=${uuid%\}*}
  NAME=$(vboxmanage showvminfo $ID --machinereadable | grep 'name=')
  GROUP=$(vboxmanage showvminfo $ID --machinereadable | grep 'groups=')
  echo "NAME=$NAME;GROUP=$GROUP"
done
